<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$kelas = addslashes(htmlentities($_GET['kelas']));
$paralel = addslashes(htmlentities($_GET['paralel']));
$tahun = addslashes(htmlentities($_GET['tahun']));
$filter = addslashes(htmlentities($_GET['filter']));

// $query = "SELECT * FROM siswa WHERE status = 1 ORDER BY nama";
$query = "  SELECT t2.* FROM grup t1 
            LEFT JOIN siswa t2 ON t1.nis = t2.nis
            WHERE (t2.nama LIKE '%$filter%' OR t2.nis LIKE '%$filter%')
                AND t1.kelas = '$kelas' 
                AND t1.paralel = '$paralel' 
                AND t1.tahun = '$tahun' 
                AND t2.status = 1 
            ORDER BY t2.nama
        ";


$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
?>