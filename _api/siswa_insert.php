<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";
  
$nis = addslashes(htmlentities($_POST["nis"]));
$nama = addslashes(htmlentities($_POST["nama"]));
$smt1_tinggi = addslashes(htmlentities($_POST["smt1_tinggi"]));
$smt1_berat = addslashes(htmlentities($_POST["smt1_berat"]));
$smt2_tinggi = addslashes(htmlentities($_POST["smt2_tinggi"]));
$smt2_berat = addslashes(htmlentities($_POST["smt2_berat"]));
$kesehatan_telinga = addslashes(htmlentities($_POST["kesehatan_telinga"]));
$kesehatan_mata = addslashes(htmlentities($_POST["kesehatan_mata"]));
$kesehatan_gigi = addslashes(htmlentities($_POST["kesehatan_gigi"]));
$kesehatan_lain = addslashes(htmlentities($_POST["kesehatan_lain"]));
$lahir_tempat = addslashes(htmlentities($_POST["lahir_tempat"]));
$lahir_tanggal = addslashes(htmlentities($_POST["lahir_tanggal"]));
$gender = addslashes(htmlentities($_POST["gender"]));
$agama_kode = addslashes(htmlentities($_POST["agama_kode"]));
$agama_nama = addslashes(htmlentities($_POST["agama_nama"]));
$pendidikan_sebelum = addslashes(htmlentities($_POST["pendidikan_sebelum"]));
$alamat = addslashes(htmlentities($_POST["alamat"]));
$ayah_nama = addslashes(htmlentities($_POST["ayah_nama"]));
$ibu_nama = addslashes(htmlentities($_POST["ibu_nama"]));
$ayah_pekerjaan = addslashes(htmlentities($_POST["ayah_pekerjaan"]));
$ibu_pekerjaan = addslashes(htmlentities($_POST["ibu_pekerjaan"]));
$orangtua_alamat = addslashes(htmlentities($_POST["orangtua_alamat"]));
$wali_nama = addslashes(htmlentities($_POST["wali_nama"]));
$wali_pekerjaan = addslashes(htmlentities($_POST["wali_pekerjaan"]));
$wali_alamat = addslashes(htmlentities($_POST["wali_alamat"]));

$query =    "   INSERT IGNORE INTO siswa 
                SET nis                 = '$nis', 
                    nama                = '$nama',
                    smt1_tinggi         = '$smt1_tinggi',
                    smt1_berat          = '$smt1_berat',
                    smt2_tinggi         = '$smt2_tinggi',
                    smt2_berat          = '$smt2_berat',
                    kesehatan_telinga   = '$kesehatan_telinga',
                    kesehatan_mata      = '$kesehatan_mata',
                    kesehatan_gigi      = '$kesehatan_gigi',
                    kesehatan_lain      = '$kesehatan_lain',
                    lahir_tempat        = '$lahir_tempat',
                    lahir_tanggal       = '$lahir_tanggal',
                    gender              = '$gender',
                    agama_kode          = '$agama_kode',
                    agama_nama          = '$agama_nama',
                    pendidikan_sebelum  = '$pendidikan_sebelum',
                    alamat              = '$alamat',
                    ayah_nama           = '$ayah_nama',
                    ibu_nama            = '$ibu_nama',
                    ayah_pekerjaan      = '$ayah_pekerjaan',
                    ibu_pekerjaan       = '$ibu_pekerjaan',
                    orangtua_alamat     = '$orangtua_alamat',
                    wali_nama           = '$wali_nama',
                    wali_pekerjaan      = '$wali_pekerjaan',
                    wali_alamat         = '$wali_alamat'
            ";

$result = mysqli_query($conn, $query);

if ($result) {
    echo json_encode(array("message" => "Sukses menambah data baru.", "status" => true));
}
else {
    echo json_encode(array("message" => "Gagal menambah data baru.", "status" => false));

}
