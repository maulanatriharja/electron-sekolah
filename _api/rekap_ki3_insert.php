<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";
 
$id_siswa = addslashes(htmlentities($_POST["id_siswa"]));
$kelas = addslashes(htmlentities($_POST["kelas"]));
$semester = addslashes(htmlentities($_POST["semester"]));
$tahun = addslashes(htmlentities($_POST["tahun"]));
$id_mapel = addslashes(htmlentities($_POST["id_mapel"]));

$query =    "   INSERT INTO rekap_ki3_insert (id_siswa, kelas, semester, tahun, id_mapel)
                SELECT * FROM (SELECT '$id_siswa', '$kelas', '$semester', '$tahun', '$id_mapel') AS tmp
                WHERE NOT EXISTS (
                    SELECT id_siswa FROM rekap_ki3_insert WHERE id_siswa = '$id_siswa'
                ) LIMIT 1;
            ";

$result = mysqli_query($conn, $query);

if ($result) {
    echo json_encode(array("message" => "Sukses menambah data baru.", "status" => true));
} else {
    echo json_encode(array("message" => "Gagal menambah data baru.", "status" => false));
}
