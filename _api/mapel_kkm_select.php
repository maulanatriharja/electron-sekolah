<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$kelas = addslashes(htmlentities($_GET['kelas']));

$query = "  SELECT * FROM
                (SELECT t1.*, COUNT(t2.id) AS jumlah_ki3  FROM mapel t1
                LEFT JOIN parameter_ki3 t2 ON t1.id=t2.id_mapel  
                WHERE t1.kelas = '1' AND t2.status = 1  
                GROUP BY t1.id
                ORDER BY t1.id) tx1
            lEFT JOIN
                (SELECT t1.id, COUNT(t2.id) AS jumlah_ki4  FROM mapel t1
                LEFT JOIN parameter_ki4 t2 ON t1.id=t2.id_mapel  
                WHERE t1.kelas = '1' AND t2.status = 1  
                GROUP BY t1.id
                ORDER BY t1.id) tx2
            ON tx1.id=tx2.id
";

$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
?>