<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
require 'connection.php';

$harian = addslashes(htmlentities($_POST['harian']));
$tengah = addslashes(htmlentities($_POST["tengah"])); 
$akhir = addslashes(htmlentities($_POST["akhir"]));

$query = "  UPDATE setting_nas 
            SET harian = '$harian', 
                semester_tengah = '$tengah',
                semester_akhir = '$akhir'
            ";

if (mysqli_query($conn, $query) or die('Update Query Failed')) {
    echo json_encode(array('message' => 'Sukses mengubah data baru.', 'status' => true));
}
else {
    echo json_encode(array('message' => 'Gagal mengubah data baru.', 'status' => false));
}
