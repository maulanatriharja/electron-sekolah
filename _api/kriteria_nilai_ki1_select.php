<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php"; 

$query = "SELECT * FROM kriteria_nilai_ki1 ORDER BY nilai DESC";

$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
?>