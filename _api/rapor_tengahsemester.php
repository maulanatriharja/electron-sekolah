<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$id_siswa = addslashes(htmlentities($_GET['id_siswa']));

$kelas = addslashes(htmlentities($_GET['kelas']));
// $semester = addslashes(htmlentities($_GET['semester']));
// $tahun = addslashes(htmlentities($_GET['tahun']));
$id_tema = addslashes(htmlentities($_GET['id_tema']));
$tipe = addslashes(htmlentities($_GET['tipe']));


$query = "  SELECT nomor, id_mapel, mapel_nama, kkm,
SUM(avg_npts) / COUNT(avg_npts) AS nilai_tengah_semester,

CASE  
    WHEN (SUM(avg_npts) / COUNT(avg_npts)) AND (SUM(avg_npts) / COUNT(avg_npts)) >= 80 THEN 'A'
    WHEN (SUM(avg_npts) / COUNT(avg_npts)) AND (SUM(avg_npts) / COUNT(avg_npts)) >= 77 THEN 'B'            
    WHEN (SUM(avg_npts) / COUNT(avg_npts)) AND (SUM(avg_npts) / COUNT(avg_npts)) >= 65 THEN 'C'            
    WHEN (SUM(avg_npts) / COUNT(avg_npts)) AND (SUM(avg_npts) / COUNT(avg_npts)) >= 0 THEN 'D'
ELSE null END 
AS predikat_tengah_semester,
             
SUM(avg_npas) / COUNT(avg_npas) AS nilai_akhir_semester,

CASE  
    WHEN (SUM(avg_npas) / COUNT(avg_npas)) AND (SUM(avg_npas) / COUNT(avg_npas)) >= 80 THEN 'A'
    WHEN (SUM(avg_npas) / COUNT(avg_npas)) AND (SUM(avg_npas) / COUNT(avg_npas)) >= 77 THEN 'B'            
    WHEN (SUM(avg_npas) / COUNT(avg_npas)) AND (SUM(avg_npas) / COUNT(avg_npas)) >= 65 THEN 'C'            
    WHEN (SUM(avg_npas) / COUNT(avg_npas)) AND (SUM(avg_npas) / COUNT(avg_npas)) >= 0 THEN 'D'
ELSE null END 
AS predikat_akhir_semester

FROM (
SELECT  t1.id AS id_mapel, t1.mapel_nama, t1.nomor, t1.kkm,
            t2.id_ki3, t2.kode,
            t3.baris,

            t4.cek_t1,
            t4.cek_t2,
            t4.cek_t3,
            t4.cek_t4,
            
            t4.nilai_nph_t1,
            t4.nilai_nph_t2,
            t4.nilai_nph_t3,
            t4.nilai_nph_t4,
            
            t4.avg_nph,

            t4.nilai_npts_t1,
            t4.nilai_npts_t2,
            
            t4.avg_npts,
            
            @pst :=
            CASE  
            WHEN t4.avg_npts AND t4.avg_npts >= 80 THEN 'A'
            WHEN t4.avg_npts AND t4.avg_npts >= 77 THEN 'B'            
            WHEN t4.avg_npts AND t4.avg_npts >= 65 THEN 'C'            
            WHEN t4.avg_npts AND t4.avg_npts >= 0 THEN 'D'
            ELSE null END 
            AS predikat_semester_tengah,
            
            IF(t4.avg_npts, 'T', null) AS t_semester_tengah,
            
            CASE  
            WHEN @pst = 'A' THEN CONCAT('sangat baik dalam ', t2.aspek)
            WHEN @pst = 'B' THEN CONCAT('baik dalam ', t2.aspek)         
            WHEN @pst = 'C' THEN CONCAT('cukup dalam ', t2.aspek)         
            WHEN @pst = 'D' THEN CONCAT('perlu bimbingan dalam ', t2.aspek)
            ELSE null END 
            AS aspek_semester_tengah,

            t4.nilai_npas_t1,
            t4.nilai_npas_t2,
            t4.nilai_npas_t3,
            t4.nilai_npas_t4,
            
            t4.avg_npas, 
            
            @nsa := 
            IF(
                t4.avg_npts,
                (t4.avg_nph * 50/100) + (t4.avg_npts * 20/100) + (t4.avg_npas * 30/100),
                (t4.avg_nph * 70/100) + (t4.avg_npas * 30/100) 
            )
            AS nilai_semester_akhir,  
            
            @psa :=
            CASE  
            WHEN @nsa AND @nsa >= 80 THEN 'A'
            WHEN @nsa AND  @nsa >= 77 THEN 'B'            
            WHEN @nsa AND @nsa >= 65 THEN 'C'         
            WHEN @nsa AND @nsa >= 0 THEN 'D'
            ELSE null END 
            AS predikat_semester_akhir,
            
            IF(@nsa, 'T', null) AS t_semester_akhir,
            
            CASE  
            WHEN @psa = 'A' THEN CONCAT('sangat baik dalam ', t2.aspek)
            WHEN @psa = 'B' THEN CONCAT('baik dalam ', t2.aspek)         
            WHEN @psa = 'C' THEN CONCAT('cukup dalam ', t2.aspek)         
            WHEN @psa = 'D' THEN CONCAT('perlu bimbingan dalam ', t2.aspek)
            ELSE null END 
            AS aspek_semester_akhir
            
            FROM (SELECT  *, ROW_NUMBER() OVER() AS nomor FROM mapel WHERE kelas='1') t1 
            LEFT JOIN (SELECT id AS id_ki3, id_mapel, kode, aspek, status FROM parameter_ki3 WHERE status=1) t2 ON t1.id=t2.id_mapel            
            LEFT JOIN (SELECT id, id_mapel, count(*) as baris FROM parameter_ki3 WHERE status=1 GROUP BY id_mapel) t3 ON t2.id_mapel=t3.id_mapel 

            LEFT JOIN (
                SELECT 	tc1.id_nilai_ki3_setting, tc1.id_ki3, tc1.cek AS cek_t1,
                        tc2.cek AS cek_t2,
                        tc3.cek AS cek_t3,
                        tc4.cek AS cek_t4,

                        tn1.nilai AS nilai_nph_t1,
                        tn2.nilai AS nilai_nph_t2,
                        tn3.nilai AS nilai_nph_t3,
                        tn4.nilai AS nilai_nph_t4,
                
                        (IFNULL(tn1.nilai, 0) + IFNULL(tn2.nilai, 0) + IFNULL(tn3.nilai, 0) + IFNULL(tn4.nilai, 0)) /
                        (IF(tc1.cek = 'true', 1, 0) + IF(tc2.cek = 'true', 1, 0) + IF(tc3.cek = 'true', 1, 0) + IF(tc4.cek = 'true', 1, 0)) AS avg_nph,
            
                        tn_npts_1.nilai AS nilai_npts_t1,
                        tn_npts_2.nilai AS nilai_npts_t2,
                
                        (IFNULL(tn_npts_1.nilai, 0) + IFNULL(tn_npts_2.nilai, 0)) /
                        (IF(tc1.cek = 'true', 1, 0) + IF(tc2.cek = 'true', 1, 0)) AS avg_npts,

                        tn_npas_1.nilai AS nilai_npas_t1,
                        tn_npas_2.nilai AS nilai_npas_t2,
                        tn_npas_3.nilai AS nilai_npas_t3,
                        tn_npas_4.nilai AS nilai_npas_t4,
                
                        (IFNULL(tn_npas_1.nilai, 0) + IFNULL(tn_npas_2.nilai, 0) + IFNULL(tn_npas_3.nilai, 0) + IFNULL(tn_npas_4.nilai, 0)) /
                        (IF(tc1.cek = 'true', 1, 0) + IF(tc2.cek = 'true', 1, 0) + IF(tc3.cek = 'true', 1, 0) + IF(tc4.cek = 'true', 1, 0)) AS avg_npas

                FROM (SELECT * FROM nilai_ki3_setting WHERE id_tema=1) tc1
                LEFT JOIN (SELECT * FROM nilai_ki3_setting WHERE id_tema=2) tc2 ON tc1.id_ki3=tc2.id_ki3
                LEFT JOIN (SELECT * FROM nilai_ki3_setting WHERE id_tema=3) tc3 ON tc1.id_ki3=tc3.id_ki3
                LEFT JOIN (SELECT * FROM nilai_ki3_setting WHERE id_tema=4) tc4 ON tc1.id_ki3=tc4.id_ki3

                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=1 AND tipe=0 AND id_siswa='2') tn1 ON tc1.id_ki3=tn1.id_ki
                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=2 AND tipe=0 AND id_siswa='2') tn2 ON tc1.id_ki3=tn2.id_ki
                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=3 AND tipe=0 AND id_siswa='2') tn3 ON tc1.id_ki3=tn3.id_ki
                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=4 AND tipe=0 AND id_siswa='2') tn4 ON tc1.id_ki3=tn4.id_ki

                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=1 AND tipe=1 AND id_siswa='2') tn_npts_1 ON tc1.id_ki3=tn_npts_1.id_ki
                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=2 AND tipe=1 AND id_siswa='2') tn_npts_2 ON tc1.id_ki3=tn_npts_2.id_ki 

                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=1 AND tipe=2 AND id_siswa='2') tn_npas_1 ON tc1.id_ki3=tn_npas_1.id_ki
                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=2 AND tipe=2 AND id_siswa='2') tn_npas_2 ON tc1.id_ki3=tn_npas_2.id_ki 
                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=3 AND tipe=2 AND id_siswa='2') tn_npas_3 ON tc1.id_ki3=tn_npas_3.id_ki
                LEFT JOIN (SELECT * FROM nilai_ki3 WHERE id_tema=4 AND tipe=2 AND id_siswa='2') tn_npas_4 ON tc1.id_ki3=tn_npas_4.id_ki 

                ORDER BY tc1.id_ki3
            ) t4 ON t2.id_ki3 = t4.id_ki3     

            WHERE t1.status=1 
            ORDER BY nomor, id_ki3, CAST(SUBSTR(kode FROM 3) AS UNSIGNED)
    ) AS tabel_rapor
    GROUP BY id_mapel
";

$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
