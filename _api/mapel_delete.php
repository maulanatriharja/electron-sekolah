<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
require 'connection.php';

$id = addslashes(htmlentities($_POST['id']));

$query = "UPDATE mapel SET status = 0 WHERE id = '$id'";

if (mysqli_query($conn, $query) or die('Delete Query Failed')) {
    echo json_encode(array('message' => 'Data berhasil dihidden.', 'status' => true));
}
else {
    echo json_encode(array('message' => 'Data gagal dihidden.', 'status' => false));
}
?>
