<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$id_nilai_ki3_setting = addslashes(htmlentities($_POST["id_nilai_ki3_setting"]));
$id_tema = addslashes(htmlentities($_POST["id_tema"]));
$id_ki3 = addslashes(htmlentities($_POST["id_ki3"]));
$cek = addslashes(htmlentities($_POST["cek"]));

$query =    "   INSERT INTO nilai_ki3_setting 
                SET id_nilai_ki3_setting = '$id_nilai_ki3_setting', id_tema = $id_tema, id_ki3 = '$id_ki3', cek = '$cek'
                ON DUPLICATE KEY UPDATE cek='$cek'
            ";

$result = mysqli_query($conn, $query);

if ($result) {
    echo json_encode(array("message" => "Sukses menambah data baru.", "status" => true));
} else {
    echo json_encode(array("message" => "Gagal menambah data baru.", "status" => false));
}
