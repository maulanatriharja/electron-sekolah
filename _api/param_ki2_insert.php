<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$kelas = addslashes(htmlentities($_POST["kelas"]));
$aspek = addslashes(htmlentities($_POST["aspek"]));

$query = "INSERT IGNORE INTO parameter_ki2 SET kelas = '$kelas', aspek = '$aspek'";

$result = mysqli_query($conn, $query);

if ($result) {
    echo json_encode(array("message" => "Sukses menambah data baru.", "status" => true));
}
else {
    echo json_encode(array("message" => "Gagal menambah data baru.", "status" => false));

}
