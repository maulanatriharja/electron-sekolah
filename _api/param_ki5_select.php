<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";


$kelas = addslashes(htmlentities($_GET['kelas']));

$query = "  SELECT *,ROW_NUMBER() OVER(PARTITION BY id_ekstra ORDER BY id) AS nomor FROM parameter_ki5 
            WHERE kelas = $kelas AND status = 1 
            ORDER BY id_ekstra, id"; 

$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
?>