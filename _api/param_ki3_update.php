<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
require 'connection.php';

$id = addslashes(htmlentities($_POST['id'])); 
$id_mapel = addslashes(htmlentities($_POST['id_mapel']));
$kode = addslashes(htmlentities($_POST['kode']));
$aspek = addslashes(htmlentities($_POST['aspek'])); 

$query = "  UPDATE parameter_ki3 
            SET id_mapel    = '$id_mapel',  
                kode        = '$kode', 
                aspek       = '$aspek' 
            WHERE id = '$id'";

if (mysqli_query($conn, $query) or die('Update Query Failed')) {
    echo json_encode(array('message' => 'Sukses mengubah data baru.', 'status' => true));
}
else {
    echo json_encode(array('message' => 'Gagal mengubah data baru.', 'status' => false));
}
