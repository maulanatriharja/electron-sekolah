<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$id_siswa = addslashes(htmlentities($_GET['id_siswa']));
$kelas = addslashes(htmlentities($_GET['kelas']));
$paralel = addslashes(htmlentities($_GET['paralel']));
$tahun = addslashes(htmlentities($_GET['tahun']));
$id_ki = addslashes(htmlentities($_GET['id_ki']));
$tipe = addslashes(htmlentities($_GET['tipe']));
$id_tema = addslashes(htmlentities($_GET['id_tema']));

$query = "  SELECT t1.*, t2.* FROM parameter_ki4 t1
            LEFT JOIN (
                SELECT ts1.id AS id_siswa, ts1.nama, 
                ts2.id AS id_nilai, ts2.id_ki, ts2.semester, ts2.tahun, ts2.nilai 
                FROM siswa ts1 
                LEFT JOIN nilai_ki4 ts2 ON ts1.id = ts2.id_siswa
                WHERE ts1.id='$id_siswa' AND ts2.tipe='$tipe' AND ts2.id_tema='$id_tema'
            ) t2 ON t1.id = t2.id_ki
        ";

$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
