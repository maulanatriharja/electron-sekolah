<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$kelas = addslashes(htmlentities($_GET['kelas']));
$id_tema = addslashes(htmlentities($_GET['id_tema']));

$query = "  SELECT t1.*, t2.jumlah_ki3 FROM mapel t1
            LEFT JOIN (
                SELECT id, id_mapel, status, count(*) as jumlah_ki3 FROM parameter_ki3 ts1
                LEFT JOIN nilai_ki3_setting ts2 ON ts1.id=ts2.id_ki3 
                WHERE ts1.status=1 AND ts2.cek='true' AND ts2.id_tema='$id_tema'
                GROUP BY id_mapel
            ) t2 ON t1.id=t2.id_mapel
            WHERE t1.kelas = '$kelas' AND t2.status = 1  
            GROUP BY t1.id
            ORDER BY t1.id
";

$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
?>