<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require "connection.php";

$kelas = addslashes(htmlentities($_GET['kelas']));
$id_tema = addslashes(htmlentities($_GET['id_tema']));

$query = "  SELECT t1.*, t2.*, t3.baris, t4.cek   
            FROM (SELECT  *, ROW_NUMBER() OVER() AS nomor FROM mapel WHERE kelas='$kelas') t1 
            LEFT JOIN (SELECT id AS id_ki3, id_mapel, kode, aspek, status FROM parameter_ki3 WHERE status=1) t2 ON t1.id=t2.id_mapel            
            LEFT JOIN (
            SELECT id, id_mapel, count(*) as baris FROM parameter_ki3 ts1
                LEFT JOIN nilai_ki3_setting ts2 ON ts1.id=ts2.id_ki3 
                WHERE ts1.status=1 AND ts2.cek='true' AND ts2.id_tema='$id_tema'
                GROUP BY id_mapel
            ) t3 ON t2.id_mapel=t3.id_mapel
            LEFT JOIN nilai_ki3_setting t4 ON t2.id_ki3=t4.id_ki3 
            WHERE t1.status=1 AND t4.id_tema='$id_tema' AND t4.cek='true'
            ORDER BY nomor, CAST(SUBSTR(kode FROM 3) AS UNSIGNED)
            ";

$result = mysqli_query($conn, $query) or die("Select Query Failed.");

while ($rows = mysqli_fetch_assoc($result)) {
    $array_data[] = $rows;
}

if (mysqli_num_rows($result) > 0) {
    echo json_encode($array_data);
}
?>