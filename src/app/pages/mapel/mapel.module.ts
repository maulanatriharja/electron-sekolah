import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapelPageRoutingModule } from './mapel-routing.module';

import { MapelPage } from './mapel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapelPageRoutingModule
  ],
  declarations: [MapelPage]
})
export class MapelPageModule {}
