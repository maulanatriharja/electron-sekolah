import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-mapel',
  templateUrl: './mapel.page.html',
  styleUrls: ['./mapel.page.scss'],
})
export class MapelPage implements OnInit {

  kelas: string = "1";
  data: any = [];

  loading = true;

  constructor(
    public alertController: AlertController,
    public global: GlobalService,
    public http: HttpClient,
  ) { }

  ngOnInit() {
    this.load_data();
  }

  load_data() {
    let url = environment.server + 'mapel_kkm_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {

      console.info('MAPEL_SELECT : ' + JSON.stringify(data))

      if (data) {
        this.data = data;
        this.loading = false;
      } else {
        this.data = [];
        this.loading = false;
      }
    }, error => {
      console.error(error);
    });
  }

  async tambah() {
    const alert = await this.alertController.create({
      header: 'Tambah Mata Pelajaran Kelas ' + this.kelas,
      mode: 'ios',
      inputs: [
        {
          name: 'mapel_nama',
          type: 'text',
          placeholder: 'Nama Mata Pelajaran'
        },
        {
          name: 'kkm',
          label: 'KKM',
          type: 'number',
          placeholder: 'KKM',
          min: 0,
          max: 3,
          attributes: {
            maxlength: 3,
          }
        },
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'mapel_insert.php';
            let body = new FormData();
            body.append('kelas', this.kelas);
            body.append('mapel_nama', val.mapel_nama);
            body.append('kkm', val.kkm);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_data();
                this.global.notif_sukses("Berhasil menambah data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }

  async edit(val_id, val_mapel_nama, val_kkm) {
    const alert = await this.alertController.create({
      header: 'Edit Mata Pelajaran',
      mode: 'ios',
      inputs: [
        {
          name: 'mapel_nama',
          type: 'text',
          value: val_mapel_nama,
          placeholder: 'Nama Mata Pelajaran'
        },
        {
          name: 'kkm',
          label: 'KKM',
          type: 'number',
          value: val_kkm,
          placeholder: 'KKM',
          min: 0,
          max: 3,
          attributes: {
            maxlength: 3,
          }
        },
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'custom-alert-cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'Hapus',
          cssClass: 'custom-alert-danger',
          handler: async () => {
            const alert = await this.alertController.create({
              message: 'Yakin untuk menghapus data ?',
              buttons: [
                {
                  text: 'Batal',
                  role: 'cancel',
                  cssClass: 'custom-alert-cancel',
                  handler: () => {
                    console.log('Confirm Cancel');
                  }
                }, {
                  text: 'Hapus',
                  cssClass: 'custom-alert-danger',
                  handler: () => {
                    console.log('Confirm Okay');

                    let url = environment.server + 'mapel_delete.php';
                    let body = new FormData();
                    body.append('id', val_id);

                    this.http.post(url, body).subscribe((res: any) => {
                      if (res.status) {
                        this.load_data();
                        this.global.notif_hapus("Berhasil menghapus data.");
                      }
                    }, error => {
                      console.error(error);
                    });
                  }
                }
              ]
            });

            await alert.present();
          }
        },
        {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'mapel_update.php';
            let body = new FormData();
            body.append('id', val_id);
            body.append('mapel_nama', val.mapel_nama);
            body.append('kkm', val.kkm);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_data();
                this.global.notif_sukses("Berhasil mengubah data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }
}
