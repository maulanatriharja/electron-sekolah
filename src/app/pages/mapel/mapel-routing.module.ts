import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapelPage } from './mapel.page';

const routes: Routes = [
  {
    path: '',
    component: MapelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapelPageRoutingModule {}
