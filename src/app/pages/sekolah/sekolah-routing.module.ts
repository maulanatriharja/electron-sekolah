import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SekolahPage } from './sekolah.page';

const routes: Routes = [
  {
    path: '',
    component: SekolahPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SekolahPageRoutingModule {}
