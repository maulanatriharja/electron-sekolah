import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sekolah',
  templateUrl: './sekolah.page.html',
  styleUrls: ['./sekolah.page.scss'],
})
export class SekolahPage implements OnInit {
  sekolah: any = [
    { param: 'Nama Sekolah', value: 'SDIT BINA INSAN LUHUR KARTASURA' },
    { param: 'Alamat Sekolah', value: 'Jl.Kampung Baru No. 03 RT03 RW 07 Pabelan, Kartasura, Sukoharjo' },
    { param: 'NPSN', value: '69980152' },
    { param: 'NSS', value: '102031112044' },
    { param: 'No.Telephon', value: '(0271) 739684' }, 
    { param: 'Desa / Kelurahan', value: 'Pabelan' },
    { param: 'Kecamatan', value: 'Kartasura' },
    { param: 'Kode Pos', value: '57169' },
    { param: 'Kabupaten / Kota', value: 'Sukoharjo' },
    { param: 'Provinsi', value: 'Jawa Tengah' },
    { param: 'Website', value: '-' },
    { param: 'E-mail', value: 'sditbinainsanluhur@gmail.com' },
  ]

  kepala:any=[
    { param: 'Nama Kepala Sekolah', value: 'Yanuar Eko Saputro, S.Pd.I'},
    { param: 'NIY', value: '19820110 1707 1 01'},
  ]

  constructor() { }

  ngOnInit() {
  }

}
