import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SekolahPageRoutingModule } from './sekolah-routing.module';

import { SekolahPage } from './sekolah.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SekolahPageRoutingModule
  ],
  declarations: [SekolahPage]
})
export class SekolahPageModule {}
