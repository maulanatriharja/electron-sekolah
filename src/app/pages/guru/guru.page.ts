import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GlobalService } from './../../services/global.service';

@Component({
  selector: 'app-guru',
  templateUrl: './guru.page.html',
  styleUrls: ['./guru.page.scss'],
})
export class GuruPage implements OnInit {

  nama_guru: string;
  niy_guru: string;

  isEdit = false;

  constructor(
    public global:GlobalService,
    public storage: Storage,
  ) { }

  ngOnInit() {
    this.storage.get('guru').then((res) => {
      if (res) {
        this.nama_guru = res.nama_guru;
        this.niy_guru = res.niy_guru;
      }
    })
  }

  simpan() {
    this.storage.set('guru', { nama_guru: this.nama_guru, niy_guru: this.niy_guru }).then(() => {
      this.isEdit = false;
      this.global.notif_sukses("Berhasil menambah data.");
    }, error => {
      console.error('Error storing item :', error);
    });
  }

}
