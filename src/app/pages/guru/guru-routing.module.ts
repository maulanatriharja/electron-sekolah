import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuruPage } from './guru.page';

const routes: Routes = [
  {
    path: '',
    component: GuruPage
  },
  {
    path: 'guru-detail',
    loadChildren: () => import('./guru-detail/guru-detail.module').then( m => m.GuruDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GuruPageRoutingModule {}
