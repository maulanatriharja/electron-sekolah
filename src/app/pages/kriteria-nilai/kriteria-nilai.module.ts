import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KriteriaNilaiPageRoutingModule } from './kriteria-nilai-routing.module';

import { KriteriaNilaiPage } from './kriteria-nilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KriteriaNilaiPageRoutingModule
  ],
  declarations: [KriteriaNilaiPage]
})
export class KriteriaNilaiPageModule {}
