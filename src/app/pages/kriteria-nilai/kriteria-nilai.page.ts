import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

import { environment } from '../../../environments/environment';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-kriteria-nilai',
  templateUrl: './kriteria-nilai.page.html',
  styleUrls: ['./kriteria-nilai.page.scss'],
})
export class KriteriaNilaiPage implements OnInit {

  data_ki1: any = [];
  data_ki2: any = [];
  data_ki3: any = [];
  data_ki4: any = [];
  data_ki5: any = [];

  constructor(
    public alertController: AlertController,
    public global: GlobalService,
    public http: HttpClient,
  ) { }

  ngOnInit() {
    this.load_ki_1();
    this.load_ki_2();
    this.load_ki_3();
    this.load_ki_4();
    this.load_ki_5();
  }

  load_ki_1() {
    let url = environment.server + 'kriteria_nilai_ki1_select.php';
    this.http.get(url).subscribe((data: any) => { 
      if (data) { this.data_ki1 = data; } else { this.data_ki1 = []; }
    }, error => { console.error(error); });
  }

  load_ki_2() {
    let url = environment.server + 'kriteria_nilai_ki2_select.php';
    this.http.get(url).subscribe((data: any) => {
      if (data) { this.data_ki2 = data; } else { this.data_ki2 = []; }
    }, error => { console.error(error); });
  }

  load_ki_3() {
    let url = environment.server + 'kriteria_nilai_ki3_select.php';
    this.http.get(url).subscribe((data: any) => {
      if (data) { this.data_ki3 = data; } else { this.data_ki3 = []; }
    }, error => { console.error(error); });
  }

  load_ki_4() {
    let url = environment.server + 'kriteria_nilai_ki4_select.php';
    this.http.get(url).subscribe((data: any) => {
      if (data) { this.data_ki4 = data; } else { this.data_ki4 = []; }
    }, error => { console.error(error); });
  }

  load_ki_5() {
    let url = environment.server + 'kriteria_nilai_ki5_select.php';
    this.http.get(url).subscribe((data: any) => {
      if (data) { this.data_ki5 = data; } else { this.data_ki5 = []; }
    }, error => { console.error(error); });
  }
}
