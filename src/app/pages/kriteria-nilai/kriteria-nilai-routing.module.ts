import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KriteriaNilaiPage } from './kriteria-nilai.page';

const routes: Routes = [
  {
    path: '',
    component: KriteriaNilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KriteriaNilaiPageRoutingModule {}
