import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RaporSemesterTengahComponent } from './rapor-semester-tengah.component';

describe('RaporSemesterTengahComponent', () => {
  let component: RaporSemesterTengahComponent;
  let fixture: ComponentFixture<RaporSemesterTengahComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RaporSemesterTengahComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RaporSemesterTengahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
