import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import * as moment from 'moment';

@Component({
  selector: 'app-rapor-semester-tengah',
  templateUrl: './rapor-semester-tengah.component.html',
  styleUrls: ['./rapor-semester-tengah.component.scss'],
})
export class RaporSemesterTengahComponent implements OnInit {

  pdfSrc: any;

  @Input() kelas: any;
  @Input() paralel: any;
  @Input() semester: any;
  @Input() tahun: any;
  @Input() id_siswa: any;

  kelas_text: any;
  semester_text: any;
  tahun_text: any;

  data_siswa: any;
  // data_rekap: any = [];
  data_nilai: any = [];

  data_total: any = [];

  nilai_avg_ts: any = 0;
  nilai_avg_as: any = 0;

  docDefinition: any;

  constructor(
    public http: HttpClient,
  ) { }

  ngOnInit() {
    moment.locale('id');
  }

  ngOnChanges() {
    if (this.kelas == 1) {
      this.kelas_text = "I (satu)"
    } else if (this.kelas == 2) {
      this.kelas_text = "II (dua)"
    } else if (this.kelas == 3) {
      this.kelas_text = "III (tiga)"
    } else if (this.kelas == 4) {
      this.kelas_text = "IV (empat)"
    } else if (this.kelas == 5) {
      this.kelas_text = "V (lima)"
    } else if (this.kelas == 6) {
      this.kelas_text = "VI (enam)"
    }

    if (this.semester == 1) {
      this.semester_text = "Ganjil"
    } else if (this.semester == 2) {
      this.semester_text = "Genap"
    }

    this.tahun_text = this.tahun + '/' + (parseInt(this.tahun) + 1);

    //-----
    let url = environment.server + 'siswa_select_1.php?id_siswa=' + this.id_siswa;

    this.http.get(url).subscribe((data: any) => {

      if (data) {
        this.data_siswa = data;
        this.load_nilai();
        // this.generatePDF();
      } else {
        this.data_siswa = null;
      }

      console.info(JSON.stringify(this.data_siswa[0]));

    }, error => {
      console.error(error);
    });

  }

  load_nilai() {

    this.data_nilai = [];

    this.data_nilai.push(
      [
        { text: 'No', style: ['tb_header'], rowSpan: 2 },
        { text: 'Muatan Pelajaran', style: ['tb_header'], rowSpan: 2 },
        { text: 'KKM', style: ['tb_header'], rowSpan: 2 },
        { text: 'Pengetahuan', style: ['tb_header'], colSpan: 3 },
        '',
        ''
      ],
      [
        '',
        '',
        '',
        { text: 'N', style: ['tb_header'] },
        { text: 'P', style: ['tb_header'] },
        { text: 'Deskripsi', style: ['tb_header'] }
      ]
    );

    let url = environment.server + 'rapor.php?id_siswa=' + this.id_siswa
      + '&kelas=' + this.kelas
      + '&semester=' + this.semester
      + '&tahun=' + this.tahun
      ;

    this.http.get(url).subscribe((data_nilai: any) => {

      if (data_nilai) {
        let url_rekap = environment.server + 'rekap_nilai_select_0.php?id_siswa=' + this.id_siswa
          + '&kelas=' + this.kelas
          + '&semester=' + this.semester
          + '&tahun=' + this.tahun
          ;
        this.http.get(url_rekap).subscribe((data_rekap: any) => {

          let deskripsi: any = '';

          for (var i = 0; i < data_nilai.length; i++) {

            let m = 0;

            for (var x = 0; x < data_rekap.length; x++) {
              if (data_nilai[i].id_mapel == data_rekap[x].id_mapel) {
 
                if (data_rekap[x].aspek_semester_tengah && m <= 3) {
                  deskripsi += data_rekap[x].aspek_semester_tengah;
                  m += 1;
                }
              }
            }

            this.data_nilai.push([
              data_nilai[i].nomor,
              data_nilai[i].mapel_nama,
              data_nilai[i].kkm,
              parseFloat(parseFloat(data_nilai[i].nilai_tengah_semester).toFixed(2)),
              data_nilai[i].predikat_tengah_semester,
              deskripsi,
            ]);

            deskripsi = '';

          }

          this.generatePDF();

        }, error => {
          console.error(error);
        });
      } else {
        this.generateEmptyPDF();
      }
    }, error => {
      console.error(error);
    });
  }

  async generateEmptyPDF() {

    pdfMake.fonts = {
      'Arial': {
        normal: 'Arial.ttf',
        bold: 'Arial Bold.ttf',
        italics: 'Arial Italic.ttf',
        bolditalics: 'Arial BoldItalic.ttf'
      }
    }

    this.docDefinition = {
      pageSize: 'A4',
      pageMargins: [30, 40, 30, 40],
      // pageOrientation: 'landscape',
      content: [
        {
          text: 'data kosong'
        },
      ],
      defaultStyle: {
        alignment: 'center',
        color: '#999',
        font: 'Arial',
        fontSize: 12,
        lineHeight: 1.2,
      },
    }

    pdfMake.createPdf(this.docDefinition).getBase64((data) => {
      this.pdfSrc = 'data:application/pdf;base64,' + data;
    });
  }

  async generatePDF() {

    pdfMake.fonts = {
      'Arial': {
        normal: 'Arial.ttf',
        bold: 'Arial Bold.ttf',
        italics: 'Arial Italic.ttf',
        bolditalics: 'Arial BoldItalic.ttf'
      }
    }

    this.docDefinition = {
      pageSize: 'A4',
      pageMargins: [30, 40, 30, 40],
      // pageOrientation: 'landscape',
      content: [
        {
          text: 'LAPORAN NILAI TENGAH SEMESTER',
          style: [{ alignment: 'center', fontSize: 14, bold: true }],
          margin: [0, 0, 0, 30],
        },
        {
          layout: 'noBorders',
          table: {
            headerRows: 1,
            widths: [120, 'auto', '*', 100, 'auto', 80],

            body: [
              ['Nama Peserta didik', ':', this.data_siswa[0].nama, 'Kelas', ':', this.kelas_text],
              ['Nomor Induk', ':', this.data_siswa[0].nis, 'Semester', ':', this.semester_text],
              ['Nama Sekolah', ':', 'SDIT BINA INSAN LUHUR\nKARTASURA', 'Tahun Pelajaran', ':', this.tahun_text],
              ['Alamat Sekolah', ':', 'Jl. Kampung Baru No.03 RT.03 Rw. 07 Pabelan, Kartasura, Sukoharjo', '', '', ''],

            ]
          }, margin: [0, 0, 0, 40]
        },
        {
          text: 'Pengetahuan KI 3',
          style: [{ bold: true }],
          margin: [0, 0, 0, 20],
        },
        {
          // layout: 'noBorders',
          table: {
            headerRows: 2,
            widths: ['auto', 100, 'auto', 'auto', 'auto', '*'],

            // body: [
            //   [
            //     { text: 'No', style: ['tb_header'], rowSpan: 2 },
            //     { text: 'Muatan Pelajaran', style: ['tb_header'], rowSpan: 2 },
            //     { text: 'KKM', style: ['tb_header'], rowSpan: 2 },
            //     { text: 'Pengetahuan', style: ['tb_header'], colSpan: 3 },
            //     '',
            //     ''
            //   ],
            //   [
            //     '',
            //     '',
            //     '',
            //     { text: 'N', style: ['tb_header'] },
            //     { text: 'P', style: ['tb_header'] },
            //     { text: 'Deskripsi', style: ['tb_header'] }
            //   ]
            // ], 

            body: this.data_nilai


          }, margin: [0, 0, 0, 40]
        },
      ],
      defaultStyle: {
        font: 'Arial',
        fontSize: 12,
        lineHeight: 1.2,
      },
      styles: {
        tb_header: {
          alignment: 'center',
          bold: true,
        }
      }
    }

    pdfMake.createPdf(this.docDefinition).getBase64((data) => {
      this.pdfSrc = 'data:application/pdf;base64,' + data;
    });
  }

  cetak() {
    pdfMake.createPdf(this.docDefinition).download('TS - ' + this.data_siswa[0].nama + ', Kelas ' + this.kelas + this.paralel + ' Semester ' + this.semester + ' ' + this.tahun + '/' + (parseInt(this.tahun) + 1) + '.pdf');
    // pdfMake.createPdf(this.docDefinition).open();
    // pdfMake.createPdf(this.docDefinition).print();
  }

  getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");
      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        resolve(dataURL);
      };
      img.onerror = error => {
        reject(error);
      };
      img.src = url;
    });
  }
}
