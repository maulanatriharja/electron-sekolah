import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-rapor',
  templateUrl: './rapor.page.html',
  styleUrls: ['./rapor.page.scss'],
})
export class RaporPage implements OnInit {

  kelas: string = '';
  paralel: string = '';
  semester: string = '1';
  tahun: string = '';
  tahun_depan: number;

  // filter: string = '';

  segment: any = 'cover';

  loading = false;

  data_siswa: any = [];

  id_siswa: any;

  constructor(
    public http: HttpClient,
  ) { }

  ngOnInit() {
    this.kelas = '1';
    this.tahun = new Date().getFullYear().toString();
    this.tahun_depan = parseInt(this.tahun) + 1;

    this.load_data_siswa();
  }

  load_data_siswa() {
    let url = environment.server + 'siswa_select.php?filter=&kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_siswa = data;
        this.loading = false;

        this.id_siswa = data[0].id;
      } else {
        this.data_siswa = [];
        this.loading = false;
      }
      // console.info(JSON.stringify(data));  
    }, error => {
      console.error(error);
    });

  }

  setTahun() {
    this.tahun_depan = parseInt(this.tahun) + 1;
  }

  sgSiswaChanged(ev) {
    this.id_siswa = ev.target.value;
  }


}
