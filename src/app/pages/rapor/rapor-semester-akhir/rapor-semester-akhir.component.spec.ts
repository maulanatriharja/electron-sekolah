import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RaporSemesterAkhirComponent } from './rapor-semester-akhir.component';

describe('RaporSemesterAkhirComponent', () => {
  let component: RaporSemesterAkhirComponent;
  let fixture: ComponentFixture<RaporSemesterAkhirComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RaporSemesterAkhirComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RaporSemesterAkhirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
