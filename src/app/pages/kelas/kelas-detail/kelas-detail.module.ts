import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KelasDetailPageRoutingModule } from './kelas-detail-routing.module';

import { KelasDetailPage } from './kelas-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KelasDetailPageRoutingModule
  ],
  declarations: [KelasDetailPage]
})
export class KelasDetailPageModule {}
