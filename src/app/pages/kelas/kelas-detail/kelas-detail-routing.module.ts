import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KelasDetailPage } from './kelas-detail.page';

const routes: Routes = [
  {
    path: '',
    component: KelasDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KelasDetailPageRoutingModule {}
