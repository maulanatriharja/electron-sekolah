import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-penilaian-setting',
  templateUrl: './penilaian-setting.page.html',
  styleUrls: ['./penilaian-setting.page.scss'],
})
export class PenilaianSettingPage implements OnInit {

  kelas: any = '1';
  id_tema: any;

  loading = false;

  data: any = [];
  data_new: any = [];

  data_tema: any = [];
  count_tema: number;


  constructor(
    public http: HttpClient,
  ) { }

  ngOnInit() {
    this.load_data();
    this.load_data_tema();
  }

  async load_data() {
    let url = environment.server + 'nilai_ki3_setting_select.php?kelas=' + this.kelas + '&id_tema=' + this.id_tema;
    this.http.get(url).subscribe((data: any) => {

      this.data = data;

      // for (var i = 0; i < data.length; i++) {
      //   if ((i > 0 && data[i - 1].id_mapel) !== data[i].id_mapel) {
      //     this.data.push({
      //       id_mapel: data[i].id_mapel,
      //       mapel_nama: data[i].mapel_nama,
      //       data: []
      //     });

      //     for (var x = 0; x < data.length; x++) {
      //       if (data[i].id_mapel == data[x].id_mapel) {
      //         this.data[i].data.push({
      //           nomor: "1",
      //           id_ki3: "1",
      //           kode: "3.1",
      //           baris: "10",
      //           cek: "false"
      //         })
      //       }
      //     }
      //   }
      // }

      console.info(JSON.stringify(this.data))

    }, error => {
      console.error(error);
    });
  }

  load_data_tema() {
    this.data_tema = [];
    this.id_tema = null;

    let url = environment.server + 'tema_select.php?kelas=' + this.kelas;
    this.http.get(url).subscribe((data: any) => {
      this.data_tema = data;
    }, error => {
      console.error(error);
    });
  }

  checking(val_id_nilai_ki3_setting, val_id_ki3, event) {

    let url = environment.server + 'nilai_ki3_setting_insert.php';
    let body = new FormData();
    body.append('id_nilai_ki3_setting', val_id_nilai_ki3_setting);
    body.append('id_ki3', val_id_ki3);
    body.append('id_tema', this.id_tema);
    body.append('cek', event.detail.checked);

    this.http.post(url, body).subscribe((res: any) => {
      if (res.status) {
        console.info('SUCCESS INSERTED')
      }
    }, error => {
      console.error(error);
    });
  }

}
