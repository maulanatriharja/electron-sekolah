import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenilaianSettingPageRoutingModule } from './penilaian-setting-routing.module';

import { PenilaianSettingPage } from './penilaian-setting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenilaianSettingPageRoutingModule
  ],
  declarations: [PenilaianSettingPage]
})
export class PenilaianSettingPageModule {}
