import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenilaianSettingPage } from './penilaian-setting.page';

const routes: Routes = [
  {
    path: '',
    component: PenilaianSettingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenilaianSettingPageRoutingModule {}
