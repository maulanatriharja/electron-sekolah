import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { KkmDetailPage } from '../kkm/kkm-detail/kkm-detail.page';

// import { Kelas1Service } from './../../services/kelas1.service';
// import { Kelas2Service } from './../../services/kelas2.service';
// import { Kelas3Service } from './../../services/kelas3.service';
// import { Kelas4Service } from './../../services/kelas4.service';
// import { Kelas5Service } from './../../services/kelas5.service';
// import { Kelas6Service } from './../../services/kelas6.service';

@Component({
  selector: 'app-kkm',
  templateUrl: './kkm.page.html',
  styleUrls: ['./kkm.page.scss'],
})
export class KkmPage implements OnInit {
  @ViewChild('slides', { static: true }) slider: IonSlides;

  kelas: any = '1';

  segment: any = 0;
  data: any = []; 

  param_ki_34: any = [];

  constructor(
    // public kelas1: Kelas1Service,
    // public kelas2: Kelas2Service,
    // public kelas3: Kelas3Service,
    // public kelas4: Kelas4Service,
    // public kelas5: Kelas5Service,
    // public kelas6: Kelas6Service,
    public modalController: ModalController,
    public storage: Storage,
  ) { }

  ngOnInit() {
    // this.param_ki_34 = this.kelas1.param_ki_34();
  }

  async segmentChanged(ev: any) {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  ionViewDidEnter() {
    this.load_data();
  }

  load_data() {
    this.storage.get('kkm').then((res) => {
      this.data = res[0].data; 
    });
  }

  async detail(val) {

    const modal = await this.modalController.create({
      component: KkmDetailPage,
      // cssClass: 'custom-modal-kkm',
      cssClass: 'auto-height',
      componentProps: {
        'kelas': this.segment + 1,
        'data': val
      }
    });

    modal.onDidDismiss().then(() => {
      this.load_data();
    });

    return await modal.present();
  }

}
