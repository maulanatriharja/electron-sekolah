import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KkmPageRoutingModule } from './kkm-routing.module';

import { KkmPage } from './kkm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KkmPageRoutingModule
  ],
  declarations: [KkmPage]
})
export class KkmPageModule {}
