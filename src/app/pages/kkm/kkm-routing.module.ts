import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KkmPage } from './kkm.page';

const routes: Routes = [
  {
    path: '',
    component: KkmPage
  },
  {
    path: 'kkm-detail',
    loadChildren: () => import('./kkm-detail/kkm-detail.module').then( m => m.KkmDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KkmPageRoutingModule {}
