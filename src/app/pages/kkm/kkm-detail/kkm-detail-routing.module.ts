import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KkmDetailPage } from './kkm-detail.page';

const routes: Routes = [
  {
    path: '',
    component: KkmDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KkmDetailPageRoutingModule {}
