import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { IonInput } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { GlobalService } from './../../../services/global.service';

import * as moment from 'moment';

@Component({
  selector: 'app-kkm-detail',
  templateUrl: './kkm-detail.page.html',
  styleUrls: ['./kkm-detail.page.scss'],
})
export class KkmDetailPage implements OnInit {
  // @ViewChild('input', { static: false }) inputElement: IonInput;

  judul: string;

  @Input() kelas: any;
  @Input() data: any;

  id_muatan_pelajaran: any;

  myForm: FormGroup;
  isSubmitted = false;
  isNewData: boolean;

  constructor(
    public formBuilder: FormBuilder,
    public global: GlobalService,
    public modalController: ModalController,
    private route: ActivatedRoute,
    public storage: Storage,
  ) { }

  ngOnInit() {

    if (this.data) {
      this.judul = "Edit KKM";
      this.id_muatan_pelajaran = this.data.id_muatan_pelajaran;
      this.isNewData = false;
      console.info("new data : " + this.isNewData);
    } else {
      this.judul = "Tambah KKM";
      this.id_muatan_pelajaran = moment().format('YYMMDDHHmmss');
      this.isNewData = true;
      console.info("new data : " + this.isNewData);
    }

    this.myForm = this.formBuilder.group({
      muatan_pelajaran: [this.data.muatan_pelajaran, Validators.required],
      nilai_minimal: [this.data.nilai_minimal, Validators.required],
    });
  }

  ngAfterViewInit() {
    // setTimeout(() => {
    //   this.inputElement.setFocus();
    // }, 400);
  }

  simpan() {
    this.isSubmitted = true;

    if (this.myForm.valid) {

      let data: any = {
        // kelas: this.kelas,
        // data: [{
        id_muatan_pelajaran: this.id_muatan_pelajaran,
        muatan_pelajaran: this.myForm.get('muatan_pelajaran').value,
        nilai_minimal: this.myForm.get('nilai_minimal').value
        // }]
      }

      this.storage.get('kkm').then((res) => {

        if (this.isNewData) { //tambah data
          if (!res) {
            res = [
              { kelas: 1, data: [] },
              { kelas: 2, data: [] },
              { kelas: 3, data: [] },
              { kelas: 4, data: [] },
              { kelas: 5, data: [] },
              { kelas: 6, data: [] },
            ];
          }

          res[this.kelas - 1].data.push(data);

          this.storage.set('kkm', res).then(() => {
            this.modalController.dismiss();
            this.storage.get('kkm').then((res) => { console.info(JSON.stringify(res)); });
          }, error => {
            console.error('Error storing item :', error);
          });
        } else { //update data
          let itemIndex = res[this.kelas - 1].data.findIndex(item => item.id_muatan_pelajaran === this.id_muatan_pelajaran);
          res[this.kelas - 1].data[itemIndex] = data;

          this.storage.set('kkm', res).then(() => {
            this.modalController.dismiss();
            this.storage.get('kkm').then((res) => { console.info(JSON.stringify(res)); });
          }, error => {
            console.error('Error storing item :', error);
          });
        }
      });
    }
  }

  hapus() { }
}
