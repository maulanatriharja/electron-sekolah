import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';

import { environment } from '../../../../environments/environment';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-ki5-nilai',
  templateUrl: './ki5-nilai.component.html',
  styleUrls: ['./ki5-nilai.component.scss'],
})
export class Ki5NilaiComponent implements OnInit {

  @Input() kelas: any = '';
  @Input() paralel: any = '';
  @Input() semester: any = '';
  @Input() tahun: any = '';
  tahun_depan: number;
  segment: any = 0;

  data_siswa: any = [];

  data_ekstra: any = [];
  data_ki: any = [];
  data_nilai: any = [];
  data_nilai_baru: any = [];

  id_ki: any;

  lebar_col1: any = 0;
  tinggi_col1: any = 0;

  loading = true;
  animasi: string;

  constructor(
    public global: GlobalService,
    public http: HttpClient,
    public loadingController: LoadingController,
  ) { }

  async segmentChanged(ev: any) {
    // this.load_data_nilai(this.data_ki[ev.target.value].id)
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.tahun_depan = parseInt(this.tahun) + 1;

    this.load_data_ki();
    this.load_data_nilai('');
    // this.load_data_nilai('');

    this.load_data_ekstra();
  }

  load_data_ekstra() {
    this.loading = true;

    this.data_ekstra = [];
    this.data_nilai = [];
    this.data_nilai_baru = [];

    let url_ekstra = environment.server + 'param_ki5_select_ekstra.php?kelas=' + this.kelas;
    this.http.get(url_ekstra).subscribe((data_ekstra: any) => {
      if (data_ekstra) {
        for (var x = 0; x < data_ekstra.length; x++) {
          let index_x = x;

          // alert(JSON.stringify(data_ekstra))

          this.data_ekstra.push({ id_ekstra: data_ekstra[x].id, ekstra_nama: data_ekstra[x].ekstra_nama, jumlah: data_ekstra[x].jumlah, aspek: [], data_nilai: [] });

          let url_aspek = environment.server + 'param_ki5_select.php?kelas=' + this.kelas;
          this.http.get(url_aspek).subscribe((data_aspek: any) => {
            
            if (data_aspek) {
              for (var i = 0; i < data_aspek.length; i++) {
                if (data_aspek[i].id_ekstra == this.data_ekstra[index_x].id_ekstra) {
                  this.data_ekstra[index_x].aspek.push({
                    aspek: data_aspek[i].aspek
                  });
                }

                if (index_x == data_ekstra.length - 1 && i == data_aspek.length - 1) {
                  this.load_data_nilai('');
                }
              }

            } else {
              this.data_ki = [];
            }
            
          }, error => {
            console.error(error);
          });
        }
      } else {
        this.data_ekstra = [];
      }
    }, error => {
      console.error(error);
    });

    // var intv_x: number = 0;
    // var intv = setInterval(() => {
    //   console.info('interpal');
    //   console.info(JSON.stringify(this.data_ekstra));

    //   intv_x += 1;

    //   if (intv_x > 3) {
    //     clearInterval(intv)
    //   }
    // }, 1000);
  }

  load_data_ki() {
    let url = environment.server + 'param_ki5_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_ki = data;

        if (this.data_nilai.length < 1) {
          // this.load_data_nilai(data[0].id)
        }
      } else {
        this.data_ki = [];
      }
    }, error => {
      console.error(error);
    });
  }

  // load_data_nilai() {

  //   let url_ekstra = environment.server + 'param_ki5_select_ekstra.php?kelas=' + this.kelas;
  //   this.http.get(url_ekstra).subscribe((data_ekstra: any) => {
  //     if (data_ekstra) {
  //       for (var x = 0; x < data_ekstra.length; x++) {
  //         let index_x = x;

  //         let url_siswa = environment.server + 'siswa_select.php?filter=&kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun;
  //         this.http.get(url_siswa).subscribe((data_siswa: any) => {

  //           for (var s = 0; s < data_siswa.length; s++) {
  //             let index_s = s;

  //             this.data_ekstra[index_x].data_nilai.push({ id_siswa: data_siswa[s].id, nama_siswa: data_siswa[s].nama, nilai: [] })

  //             let url = environment.server + 'nilai_ki5_select.php?kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun + '&id_ki=&id_siswa=' + data_siswa[s].id + '&id_ekstra=' + this.data_ekstra[index_x].id_ekstra;
  //             this.http.get(url).subscribe((data_nilai: any) => {

  //               for (var i = 0; i < data_nilai.length; i++) {
  //                 this.data_ekstra[index_x].data_nilai[index_s].nilai.push({
  //                   id: data_nilai[i].id,
  //                   id_ekstra: data_nilai[i].id_ekstra,
  //                   // kelas: data_nilai[i].kelas,
  //                   aspek: data_nilai[i].aspek,
  //                   // status: data_nilai[i].status,
  //                   id_siswa: data_nilai[i].id_siswa,
  //                   // nama: data_nilai[i].nama,
  //                   id_nilai: data_nilai[i].id_nilai,
  //                   id_ki: data_nilai[i].id_ki,
  //                   // semester: data_nilai[i].semester,
  //                   // tahun: data_nilai[i].tahun,
  //                   nilai: data_nilai[i].nilai,
  //                 });

  //                 if (index_x == data_siswa.length - 1 && i == data_nilai.length - 1) {
  //                   this.loading = false;
  //                 }
  //               }

  //             }, error => {
  //               console.error(error);
  //             });
  //           }

  //         }, error => {
  //           console.error(error);
  //         });


  //       }
  //     } else {
  //       this.data_ekstra = [];
  //     }
  //   }, error => {
  //     console.error(error);
  //   });
  // }

  load_data_nilai(val_id_ki) {
    this.data_nilai = [];

    this.loading = true;

    let url = environment.server + 'siswa_select.php?filter=&kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun;
    this.http.get(url).subscribe((data_siswa: any) => {

      this.data_siswa = data_siswa;

      if (data_siswa) {
        for (var x = 0; x < data_siswa.length; x++) {
          let index_x = x;

          this.data_nilai.push({ id_siswa: data_siswa[x].id, nama_siswa: data_siswa[x].nama, nilai: [] })

          let url = environment.server + 'nilai_ki5_select.php?kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun + '&id_ki=' + val_id_ki + '&id_siswa=' + data_siswa[x].id;
          this.http.get(url).subscribe((data: any) => {

            if (data) {
              for (var i = 0; i < data.length; i++) {
                this.data_nilai[index_x].nilai.push({
                  id: data[i].id,
                  id_ekstra: data[i].id_ekstra,
                  // kelas: data[i].kelas,
                  aspek: data[i].aspek,
                  // status: data[i].status,
                  id_siswa: data[i].id_siswa,
                  // nama: data[i].nama,
                  id_nilai: data[i].id_nilai,
                  id_ki: data[i].id_ki,
                  // semester: data[i].semester,
                  // tahun: data[i].tahun,
                  nilai: data[i].nilai,
                });

                if (index_x == data_siswa.length - 1 && i == data.length - 1) {

                  //-----

                  var intv_x: number = 0;
                  var intv_lebar = setInterval(() => {
                    if (document.getElementById("lebar_col1")) {
                      this.lebar_col1 = document.getElementById("lebar_col1").clientWidth;

                      console.info('interpal');

                      intv_x += 1;

                      if (intv_x > 3) {
                        clearInterval(intv_lebar)
                      }
                    }
                  }, 100);

                  var intv_y: number = 0;
                  var intv_tinggi = setInterval(() => {
                    if (document.getElementById("tinggi_col1")) {
                      this.tinggi_col1 = document.getElementById("tinggi_col1").clientHeight;

                      console.info('interpal');

                      intv_y += 1;

                      if (intv_y > 3) {
                        clearInterval(intv_tinggi)
                      }
                    }
                  }, 100);

                  //-----

                  this.loading = false;
                }
              }
            } else {
              this.loading = false;
            }

          }, error => {
            console.error(error);
          });

        }
      } else {
        this.data_siswa = [];
        this.loading = false;
      }
    }, error => {
      console.error(error);
    });

    var intv_x: number = 0;
    var intv = setInterval(() => {
      console.info('interpal');
      console.info(JSON.stringify(this.data_nilai));

      intv_x += 1;

      if (intv_x > 3) {
        clearInterval(intv)
      }
    }, 100);
  }

  numberOnly(event: any) {
    if (event.keyCode === 9) {
      // event.preventDefault();
      event.preventDefault();
      alert(8)
    }

    const pattern = /[0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  edit(val_id_ki, val_id_nilai, val_id_siswa, val_nilai) {
    let cek_duplikat: any = this.data_nilai_baru.filter(item => {
      return (item.id_nilai == val_id_nilai && item.id_ki == val_id_ki && item.id_siswa == val_id_siswa);
    });

    if (cek_duplikat.length > 0) {
      let index = this.data_nilai_baru.findIndex(item => {
        return (item.id_nilai === val_id_nilai && item.id_ki === val_id_ki && item.id_siswa === val_id_siswa)
      });

      if (index > -1) {
        this.data_nilai_baru.splice(index, 1, {
          id_ki: val_id_ki,
          id_nilai: val_id_nilai,
          id_siswa: val_id_siswa,
          nilai: val_nilai
        });
      }
    } else {
      this.data_nilai_baru.push({
        id_ki: val_id_ki,
        id_nilai: val_id_nilai,
        id_siswa: val_id_siswa,
        nilai: val_nilai,
      });
    }
  }

  async simpan() {
    if (this.data_nilai_baru.length > 0) {
      const loading = await this.loadingController.create({
        message: 'Menyimpan data'
      });
      await loading.present();

      for (var i = 0; i < this.data_nilai_baru.length; i++) {

        // alert(this.data_nilai_baru[i].id);
        // alert(i);

        let url = environment.server + 'nilai_ki5_insert.php';
        let body = new FormData();
        body.append('id', this.data_nilai_baru[i].id_nilai);
        body.append('id_siswa', this.data_nilai_baru[i].id_siswa);
        body.append('id_ki', this.data_nilai_baru[i].id_ki);
        body.append('semester', this.semester);
        body.append('tahun', this.tahun);
        body.append('nilai', this.data_nilai_baru[i].nilai);

        this.http.post(url, body).subscribe((res: any) => {
          if (res.status) {
            this.global.notif_sukses("Berhasil menyimpan data.");
          }
        }, error => {
          console.error(error);
        });

        if (i == (this.data_nilai_baru.length - 1)) {
          setTimeout(() => {
            // this.load_data_nilai(this.id_ki);
            this.load_data_ekstra();
            loading.dismiss();
          }, 1000)
        }
      }
    }
  }

}