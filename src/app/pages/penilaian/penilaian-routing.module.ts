import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenilaianPage } from './penilaian.page';

const routes: Routes = [
  {
    path: '',
    component: PenilaianPage
  },
  {
    path: 'ki3-nilai-detail/:kelas/:paralel/:semester/:tahun/:tipe/:tema/:id_tema',
    loadChildren: () => import('./ki3-nilai/ki3-nilai-detail/ki3-nilai-detail.module').then(m => m.Ki3NilaiDetailPageModule)
  },
  {
    path: 'ki4-nilai-detail/:kelas/:paralel/:semester/:tahun/:tipe/:tema',
    loadChildren: () => import('./ki4-nilai/ki4-nilai-detail/ki4-nilai-detail.module').then(m => m.Ki4NilaiDetailPageModule)
  },
  {
    path: 'ki3-rekap/:kelas/:semester/:tahun',
    loadChildren: () => import('./ki3-nilai/ki3-rekap/ki3-rekap.module').then(m => m.Ki3RekapPageModule)
  },
  {
    path: 'ki4-rekap/:kelas/:semester/:tahun',
    loadChildren: () => import('./ki4-nilai/ki4-rekap/ki4-rekap.module').then(m => m.Ki4RekapPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenilaianPageRoutingModule { }
