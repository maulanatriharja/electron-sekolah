import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ki4NilaiDetailPage } from './ki4-nilai-detail.page';

const routes: Routes = [
  {
    path: '',
    component: Ki4NilaiDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ki4NilaiDetailPageRoutingModule {}
