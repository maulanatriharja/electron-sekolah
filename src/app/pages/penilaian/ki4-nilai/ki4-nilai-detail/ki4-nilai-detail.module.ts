import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ki4NilaiDetailPageRoutingModule } from './ki4-nilai-detail-routing.module';

import { Ki4NilaiDetailPage } from './ki4-nilai-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ki4NilaiDetailPageRoutingModule
  ],
  declarations: [Ki4NilaiDetailPage]
})
export class Ki4NilaiDetailPageModule {}
