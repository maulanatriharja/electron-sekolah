import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ki4RekapPage } from './ki4-rekap.page';

const routes: Routes = [
  {
    path: '',
    component: Ki4RekapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ki4RekapPageRoutingModule {}
