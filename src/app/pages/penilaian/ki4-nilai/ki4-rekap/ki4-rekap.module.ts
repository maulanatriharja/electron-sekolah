import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ki4RekapPageRoutingModule } from './ki4-rekap-routing.module';

import { Ki4RekapPage } from './ki4-rekap.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ki4RekapPageRoutingModule
  ],
  declarations: [Ki4RekapPage]
})
export class Ki4RekapPageModule {}
