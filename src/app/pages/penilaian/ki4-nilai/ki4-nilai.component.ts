import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-ki4-nilai',
  templateUrl: './ki4-nilai.component.html',
  styleUrls: ['./ki4-nilai.component.scss'],
})
export class Ki4NilaiComponent implements OnInit {

  @Input() kelas: any = '';
  @Input() paralel: any = '';
  @Input() semester: any = '';
  @Input() tahun: any = '';

  data_tema: any = [];

  constructor(
    public global: GlobalService,
    public http: HttpClient,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.load_tema();
  }

  load_tema() {
    let url = environment.server + 'tema_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_tema = data;
      } else {
        this.data_tema = [];
      }
    }, error => {
      console.error(error);
    });
  }

  detail(val_tipe, val_tema) {
    this.router.navigate(['/penilaian/ki4-nilai-detail', this.kelas, this.paralel, this.semester, this.tahun, val_tipe, val_tema]);
  }

}
