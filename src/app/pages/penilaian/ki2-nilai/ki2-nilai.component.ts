import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';

import { environment } from '../../../../environments/environment';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-ki2-nilai',
  templateUrl: './ki2-nilai.component.html',
  styleUrls: ['./ki2-nilai.component.scss'],
})
export class Ki2NilaiComponent implements OnInit {

  @Input() kelas: any = '';
  @Input() paralel: any = '';
  @Input() semester: any = '';
  @Input() tahun: any = '';

  segment: any = 0;

  data_siswa: any = [];

  data_ki: any = [];
  data_nilai_simple: any = [];
  data_nilai: any = [];
  data_nilai_baru: any = [];

  id_ki: any;

  loading = true;
  animasi: string;

  constructor(
    public global: GlobalService,
    public http: HttpClient,
    public loadingController: LoadingController,
  ) { }

  async segmentChanged(ev: any) {
    this.load_data_nilai(this.data_ki[ev.target.value].id)
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.load_data_ki();
    this.load_data_nilai('');
  }

  load_data_ki() {
    this.data_siswa = [];
    this.data_ki = [];
    this.data_nilai_simple = [];
    this.data_nilai = [];
    this.data_nilai_baru = [];

    let url = environment.server + 'param_ki2_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_ki = data;

        if (this.data_nilai.length < 1) {
          // this.load_data_nilai(data[0].id)
        }
      } else {
        this.data_ki = [];
      }
    }, error => {
      console.error(error);
    });
  }

  load_data_nilai(val_id_ki) {
    this.loading = true;

    let url = environment.server + 'siswa_select.php?filter=&kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun;
    this.http.get(url).subscribe((data_siswa: any) => {

      this.data_siswa = data_siswa;

      if (data_siswa) {
        for (var x = 0; x < data_siswa.length; x++) {
          let index_x = x;

          this.data_nilai.push({ id_siswa: data_siswa[x].id, nama_siswa: data_siswa[x].nama, nilai: [] })

          let url = environment.server + 'nilai_ki2_select.php?kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun + '&id_ki=' + val_id_ki + '&id_siswa=' + data_siswa[x].id;
          this.http.get(url).subscribe((data: any) => {

            if (data) {
              for (var i = 0; i < data.length; i++) {
                this.data_nilai[index_x].nilai.push({
                  id: data[i].id,
                  // kelas: data[i].kelas,
                  aspek: data[i].aspek,
                  // status: data[i].status,
                  id_siswa: data[i].id_siswa,
                  // nama: data[i].nama,
                  id_nilai: data[i].id_nilai,
                  id_ki: data[i].id_ki,
                  // semester: data[i].semester,
                  // tahun: data[i].tahun,
                  nilai: data[i].nilai,
                });

                if (index_x == data_siswa.length - 1 && i == data.length - 1) {
                  this.loading = false;
                }
              }
            } else {
              this.loading = false;
            }

          }, error => {
            console.error(error);
          });


        }
      } else {
        this.data_siswa=[];
        this.loading = false;
      }

    }, error => {
      console.error(error);
    });


    var intv_x: number = 0;
    var intv = setInterval(() => {
      console.info('interpal');
      console.info(JSON.stringify(this.data_nilai));

      intv_x += 1;

      if (intv_x > 3) {
        clearInterval(intv)
      }
    }, 100);
  }

  numberOnly(event: any) {
    if (event.keyCode === 9) {
      // event.preventDefault();
      event.preventDefault();
      alert(8)
    }

    const pattern = /[0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  edit(val_id_ki, val_id_nilai, val_id_siswa, val_nilai) {
    let cek_duplikat: any = this.data_nilai_baru.filter(item => {
      return (item.id_nilai == val_id_nilai && item.id_ki == val_id_ki && item.id_siswa == val_id_siswa);
    });

    if (cek_duplikat.length > 0) {
      let index = this.data_nilai_baru.findIndex(item => {
        return (item.id_nilai === val_id_nilai && item.id_ki === val_id_ki && item.id_siswa === val_id_siswa)
      });

      if (index > -1) {
        this.data_nilai_baru.splice(index, 1, {
          id_ki: val_id_ki,
          id_nilai: val_id_nilai,
          id_siswa: val_id_siswa,
          nilai: val_nilai
        });
      }
    } else {
      this.data_nilai_baru.push({
        id_ki: val_id_ki,
        id_nilai: val_id_nilai,
        id_siswa: val_id_siswa,
        nilai: val_nilai,
      });
    }
  }

  async simpan() {
    // alert(JSON.stringify(this.data_nilai_baru));

    const loading = await this.loadingController.create({
      message: 'Menyimpan data'
    });
    await loading.present();

    for (var i = 0; i < this.data_nilai_baru.length; i++) {

      // alert(this.data_nilai_baru[i].id);
      // alert(i);

      let url = environment.server + 'nilai_ki2_insert.php';
      let body = new FormData();
      body.append('id', this.data_nilai_baru[i].id_nilai);
      body.append('id_siswa', this.data_nilai_baru[i].id_siswa);
      body.append('id_ki', this.data_nilai_baru[i].id_ki);
      body.append('semester', this.semester);
      body.append('tahun', this.tahun);
      body.append('nilai', this.data_nilai_baru[i].nilai);

      this.http.post(url, body).subscribe((res: any) => {
        if (res.status) {
          this.global.notif_sukses("Berhasil menambah data.");
        }
      }, error => {
        console.error(error);
      });

      if (i == (this.data_nilai_baru.length - 1)) {
        setTimeout(() => {
          this.data_nilai = [];
          this.data_nilai_baru = [];
          this.load_data_nilai(this.id_ki);
          loading.dismiss();
        }, 1000)
      }
    }
  }

}
