import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';

import { ActivatedRoute } from '@angular/router';

import { environment } from '../../../../../environments/environment';
import { GlobalService } from '../../../../services/global.service';

@Component({
  selector: 'app-ki3-nilai-detail',
  templateUrl: './ki3-nilai-detail.page.html',
  styleUrls: ['./ki3-nilai-detail.page.scss'],
})
export class Ki3NilaiDetailPage implements OnInit {

  judul: any;

  kelas: any;
  paralel: any;
  semester: any;
  tahun: any;
  tipe: any;
  tema_nama: any;
  id_tema: any;

  segment: any = 0;

  data_mapel: any = [];
  data_nilai: any = [];
  data_nilai_baru: any = [];

  id_mapel: any;

  loading = true;

  data_siswa: any = [];

  data_ki: any = [];

  id_ki: any;

  lebar_col1: any = 0;
  tinggi_col1: any = 0;

  animasi: string;

  constructor(
    public global: GlobalService,
    public http: HttpClient,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
  ) { }



  ngOnInit() {
    this.kelas = this.route.snapshot.paramMap.get('kelas');
    this.paralel = this.route.snapshot.paramMap.get('paralel');
    this.semester = this.route.snapshot.paramMap.get('semester');
    this.tahun = this.route.snapshot.paramMap.get('tahun');
    this.tipe = this.route.snapshot.paramMap.get('tipe');
    this.tema_nama = this.route.snapshot.paramMap.get('tema');
    this.id_tema = this.route.snapshot.paramMap.get('id_tema');

    if (this.tipe == 0) {
      this.judul = "Harian - " + this.tema_nama
    } else if (this.tipe == 1) {
      this.judul = "Tengah Semester - " + this.tema_nama
    } else if (this.tipe == 2) {
      this.judul = "Akhir Semester - " + this.tema_nama
    }

    this.load_data_mapel();
    this.load_data_ki();
  }

  load_data_mapel() {
    this.loading = true;

    this.data_mapel = [];
    this.data_nilai = [];
    this.data_nilai_baru = [];

    let url_mapel = environment.server + 'mapel_select_active.php?kelas=' + this.kelas + '&id_tema=' + this.id_tema;
    this.http.get(url_mapel).subscribe((data_mapel: any) => {
      if (data_mapel) {
        for (var x = 0; x < data_mapel.length; x++) {

          this.data_mapel.push({ id_mapel: data_mapel[x].id, mapel_nama: data_mapel[x].mapel_nama, jumlah: data_mapel[x].jumlah_ki3, aspek: [], data_nilai: [] });

          if (x == data_mapel.length - 1) {
            this.load_data_nilai('');
          }
        }
      } else {
        this.data_mapel = [];
        this.loading = false;
      }
    }, error => {
      console.error(error);
    });
  }

  load_data_ki() {
    let url = environment.server + 'param_ki3_select_active.php?kelas=' + this.kelas + '&id_tema=' + this.id_tema;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_ki = data;

        if (this.data_nilai.length < 1) {
          // this.load_data_nilai(data[0].id)
        }
      } else {
        this.data_ki = [];
      }
    }, error => {
      console.error(error);
    });
  }

  load_data_nilai(val_id_ki) {
    this.loading = true;

    let url = environment.server + 'siswa_select.php?filter=&kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun;
    this.http.get(url).subscribe((data_siswa: any) => {

      for (var x = 0; x < data_siswa.length; x++) {
        let index_x = x;

        this.data_nilai.push({ id_siswa: data_siswa[x].id, nama_siswa: data_siswa[x].nama, nilai: [] })

        let url = environment.server + 'nilai_ki3_select.php?kelas=' + this.kelas
          + '&paralel=' + this.paralel + '&tahun=' + this.tahun + '&id_ki=' + val_id_ki + '&id_siswa=' + data_siswa[x].id
          + '&tipe=' + this.tipe + '&id_tema=' + this.id_tema;
        this.http.get(url).subscribe((data: any) => {

          if (data) {
            for (var i = 0; i < data.length; i++) {
              this.data_nilai[index_x].nilai.push({
                id: data[i].id,
                id_mapel: data[i].id_mapel,
                // kelas: data[i].kelas,
                aspek: data[i].aspek,
                // status: data[i].status,
                id_siswa: data[i].id_siswa,
                // nama: data[i].nama,
                id_nilai: data[i].id_nilai,
                id_ki: data[i].id_ki,
                // semester: data[i].semester,
                // tahun: data[i].tahun,
                nilai: data[i].nilai,
              });

              if (index_x == data_siswa.length - 1 && i == data.length - 1) {

                //-----

                var intv_x: number = 0;
                var intv_lebar = setInterval(() => {
                  if (document.getElementById("lebar_col1")) {
                    this.lebar_col1 = document.getElementById("lebar_col1").clientWidth;

                    console.info('interpal');

                    intv_x += 1;

                    if (intv_x > 3) {
                      clearInterval(intv_lebar)
                    }
                  }
                }, 100);

                var intv_y: number = 0;
                var intv_tinggi = setInterval(() => {
                  if (document.getElementById("tinggi_col1")) {
                    this.tinggi_col1 = document.getElementById("tinggi_col1").clientHeight;

                    console.info('interpal');

                    intv_y += 1;

                    if (intv_y > 3) {
                      clearInterval(intv_tinggi)
                    }
                  }
                }, 100);

                //-----

                this.loading = false;
              }
            }
          }

        }, error => {
          console.error(error);
        });

      }

    }, error => {
      console.error(error);
    });

    var intv_x: number = 0;
    var intv = setInterval(() => {
      console.info('interpal');
      console.info(JSON.stringify(this.data_nilai));

      intv_x += 1;

      if (intv_x > 3) {
        clearInterval(intv)
      }
    }, 100);
  }

  numberOnly(event: any) {
    if (event.keyCode === 9) {
      // event.preventDefault();
      event.preventDefault();
      alert(8)
    }

    const pattern = /[0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  edit(val_id_ki, val_id_nilai, val_id_siswa, val_nilai) {
    let cek_duplikat: any = this.data_nilai_baru.filter(item => {
      return (item.id_nilai == val_id_nilai && item.id_ki == val_id_ki && item.id_siswa == val_id_siswa);
    });

    if (cek_duplikat.length > 0) {
      let index = this.data_nilai_baru.findIndex(item => {
        return (item.id_nilai === val_id_nilai && item.id_ki === val_id_ki && item.id_siswa === val_id_siswa)
      });

      if (index > -1) {
        this.data_nilai_baru.splice(index, 1, {
          id_ki: val_id_ki,
          id_nilai: val_id_nilai,
          id_siswa: val_id_siswa,
          nilai: val_nilai
        });
      }
    } else {
      this.data_nilai_baru.push({
        id_ki: val_id_ki,
        id_nilai: val_id_nilai,
        id_siswa: val_id_siswa,
        nilai: val_nilai,
      });
    }
  }

  async simpan() {
    if (this.data_nilai_baru.length > 0) {
      const loading = await this.loadingController.create({
        message: 'Menyimpan data'
      });
      await loading.present();

      for (var i = 0; i < this.data_nilai_baru.length; i++) {

        let url = environment.server + 'nilai_ki3_insert.php';
        let body = new FormData();
        body.append('id', this.data_nilai_baru[i].id_nilai);
        body.append('id_siswa', this.data_nilai_baru[i].id_siswa);
        body.append('id_ki', this.data_nilai_baru[i].id_ki);
        body.append('semester', this.semester);
        body.append('tahun', this.tahun);
        body.append('nilai', this.data_nilai_baru[i].nilai);
        body.append('tipe', this.tipe);
        body.append('id_tema', this.id_tema);

        this.http.post(url, body).subscribe((res: any) => {
          if (res.status) {
            this.global.notif_sukses("Berhasil menyimpan data.");
          }
        }, error => {
          console.error(error);
        });

        if (i == (this.data_nilai_baru.length - 1)) {
          setTimeout(() => {
            this.load_data_mapel();
            loading.dismiss();
          }, 1000)
        }
      }
    }
  }

}