import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ki3NilaiDetailPageRoutingModule } from './ki3-nilai-detail-routing.module';

import { Ki3NilaiDetailPage } from './ki3-nilai-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ki3NilaiDetailPageRoutingModule
  ],
  declarations: [Ki3NilaiDetailPage]
})
export class Ki3NilaiDetailPageModule {}
