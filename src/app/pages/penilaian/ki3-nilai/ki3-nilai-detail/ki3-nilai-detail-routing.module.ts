import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ki3NilaiDetailPage } from './ki3-nilai-detail.page';

const routes: Routes = [
  {
    path: '',
    component: Ki3NilaiDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ki3NilaiDetailPageRoutingModule {}
