import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { LoadingController } from '@ionic/angular';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax


@Component({
  selector: 'app-ki3-rekap',
  templateUrl: './ki3-rekap.page.html',
  styleUrls: ['./ki3-rekap.page.scss'],
})
export class Ki3RekapPage implements OnInit {

  kelas: any;
  semester: any;
  tahun: any;
  tahun_depan: any;

  loading = false;

  data_siswa: any = [];
  data_rekap: any = [];

  id_siswa: any;

  data_tema: any = [];
  count_tema: number;

  p_harian: number;
  p_tengah: number;
  p_akhir: number;

  nilai_avg_ts: any = 0;
  nilai_avg_as: any = 0;

  constructor(
    public http: HttpClient,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    public storage: Storage,
  ) { }

  ngOnInit() {
    this.kelas = this.route.snapshot.paramMap.get('kelas');
    this.semester = this.route.snapshot.paramMap.get('semester');
    this.tahun = this.route.snapshot.paramMap.get('tahun');
    this.tahun_depan = parseInt(this.route.snapshot.paramMap.get('tahun')) + 1
  }

  ngOnChanges() {
    alert()
    this.load_siswa();
    this.load_tema();
  }

  ionViewDidEnter() {
    this.tahun = new Date().getFullYear().toString();
    this.tahun_depan = parseInt(this.tahun) + 1;

    this.load_siswa();
    // this.load_rekap();
    this.load_tema();
    this.load_setting_nas();
  }

  load_siswa() {
    let url = environment.server + 'siswa_select.php?filter=&kelas=' + this.kelas + '&paralel=&tahun=' + this.tahun;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_siswa = data;
        this.loading = false;

        this.id_siswa = data[0].id;
      } else {
        this.data_siswa = [];
        this.loading = false;
      }
    }, error => {
      console.error(error);
    });
  }

  load_setting_nas() {
    let url = environment.server + 'setting_nas_select.php';

    this.http.get(url).subscribe((data: any) => {

      this.p_harian = data[0].harian;
      this.p_tengah = data[0].semester_tengah;
      this.p_akhir = data[0].semester_akhir;

    }, error => {
      console.error(error);
    });
  }

  load_tema() {
    let url = environment.server + 'tema_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_tema = data;

        this.count_tema = data.length;
      } else {
        this.data_tema = [];
      }
    }, error => {
      console.error(error);
    });
  }

  sgSiswaChanged(ev) {
    this.id_siswa = ev.target.value;

    this.load_rekap()
  }

  async load_rekap() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    this.data_rekap = [];

    let url = environment.server + 'rekap_nilai_select_0.php?id_siswa=' + this.id_siswa
      + '&kelas=' + this.kelas
      + '&semester=' + this.route.snapshot.paramMap.get('semester')
      + '&tahun=' + this.tahun
      ;
    this.http.get(url).subscribe((data: any) => {

      // this.data_rekap = data;

      let baris_ts = 0;

      if (data) {
        for (var i = 0; i < data.length; i++) {

          if (i == 0 && (data[0].cek_t1 == 'true' || data[0].cek_t2 == 'true')) {
            baris_ts = 1;
          } else if (i > 0 && (data[i].id_mapel == data[i - 1].id_mapel)) {
            if (data[i].cek_t1 == 'true' || data[i].cek_t2 == 'true') {
              baris_ts = baris_ts + 1;
            }
          } else {
            if (data[i].cek_t1 == 'true' || data[i].cek_t2 == 'true') {
              baris_ts = 1;
            } else {
              baris_ts = 0;
            }
          }

          this.data_rekap.push({
            id_mapel: data[i].id_mapel,
            mapel_nama: data[i].mapel_nama,
            nomor: data[i].nomor,
            id_ki3: data[i].id_ki3,
            kode: data[i].kode,
            baris: data[i].baris,

            cek_t1: data[i].cek_t1,
            cek_t2: data[i].cek_t2,
            cek_t3: data[i].cek_t3,
            cek_t4: data[i].cek_t4,

            nilai_nph_t1: data[i].nilai_nph_t1,
            nilai_nph_t2: data[i].nilai_nph_t2,
            nilai_nph_t3: data[i].nilai_nph_t3,
            nilai_nph_t4: data[i].nilai_nph_t4,
            jumlah_nph: parseFloat(parseFloat(data[i].avg_nph).toFixed(2)),

            nilai_npts_t1: data[i].nilai_npts_t1,
            nilai_npts_t2: data[i].nilai_npts_t2,
            jumlah_npts: parseFloat(parseFloat(data[i].avg_npts).toFixed(2)),

            nilai_npas_t1: data[i].nilai_npas_t1,
            nilai_npas_t2: data[i].nilai_npas_t2,
            nilai_npas_t3: data[i].nilai_npas_t3,
            nilai_npas_t4: data[i].nilai_npas_t4,
            jumlah_npas: parseFloat(parseFloat(data[i].avg_npas).toFixed(2)),

            nilai_semester_akhir: parseFloat(parseFloat(data[i].nilai_semester_akhir).toFixed(2)),
            predikat_semester_akhir: data[i].predikat_semester_akhir,
            tb_semester_akhir: data[i].t_semester_akhir,
            deskripsi_semester_akhir: data[i].aspek_semester_akhir,

            nilai_semester_tengah: parseFloat(parseFloat(data[i].avg_npts).toFixed(2)),
            predikat_semester_tengah: data[i].predikat_semester_tengah,
            tb_semester_tengah: data[i].t_semester_tengah,
            deskripsi_semester_tengah: data[i].aspek_semester_tengah,
          });

          if (i == 0) {
            this.nilai_avg_ts = (data[0].avg_npts * 1);
          } else if (i > 0 && data[i].id_mapel == data[i - 1].id_mapel) {
            this.nilai_avg_ts += ((data[i].avg_npts * 1));
          } else {
            this.nilai_avg_ts = (data[i].avg_npts * 1);
          }

          if (i == 0) {
            this.nilai_avg_as = (data[0].nilai_semester_akhir * 1) / data[0].baris;
          } else if (i > 0 && data[i].id_mapel == data[i - 1].id_mapel) {
            this.nilai_avg_as += ((data[i].nilai_semester_akhir * 1) / data[i].baris);
          } else {
            this.nilai_avg_as = (data[i].nilai_semester_akhir * 1) / data[i].baris;
          }

          if (data[i].id_mapel != data[i + 1].id_mapel) {
            this.data_rekap.push({
              total: true,
              nilai_ts: parseFloat(parseFloat(this.nilai_avg_ts).toFixed(2)) / baris_ts,
              nilai_as: parseFloat(parseFloat(this.nilai_avg_as).toFixed(2)),
            });
          }

        }
      } else {
        this.data_rekap = [];
      }
    }, error => {
      console.error(error);
    }).add(() => {

    })

    var intv_x: number = 0;
    var intv = setInterval(() => {
      // console.info('interpal');
      console.info(JSON.stringify(this.data_rekap));

      intv_x += 1;

      if (intv_x > 10) {
        clearInterval(intv)
      }
    }, 100);
  }

  setKelas() {
    this.load_siswa();
    this.load_tema();
  }

}
