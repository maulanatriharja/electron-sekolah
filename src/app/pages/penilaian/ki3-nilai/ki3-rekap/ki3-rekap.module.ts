import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ki3RekapPageRoutingModule } from './ki3-rekap-routing.module';

import { Ki3RekapPage } from './ki3-rekap.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ki3RekapPageRoutingModule
  ],
  declarations: [Ki3RekapPage]
})
export class Ki3RekapPageModule {}
