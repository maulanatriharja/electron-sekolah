import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ki3RekapPage } from './ki3-rekap.page';

const routes: Routes = [
  {
    path: '',
    component: Ki3RekapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ki3RekapPageRoutingModule {}
