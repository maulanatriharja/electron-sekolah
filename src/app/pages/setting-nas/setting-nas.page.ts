import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";

import { environment } from '../../../environments/environment';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-setting-nas',
  templateUrl: './setting-nas.page.html',
  styleUrls: ['./setting-nas.page.scss'],
})
export class SettingNasPage implements OnInit {

  myForm: FormGroup;
  isSubmitted = false;

  btn_submit_aktif = false;

  constructor(
    public formBuilder: FormBuilder,
    public global: GlobalService,
    public http: HttpClient,
  ) {
    this.myForm = new FormGroup({
      harian: new FormControl('', Validators.required),
      tengah: new FormControl('', Validators.required),
      akhir: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.load_data();
  }

  load_data() {
    let url = environment.server + 'setting_nas_select.php';

    this.http.get(url).subscribe((data: any) => {

      this.myForm = new FormGroup({
        harian: new FormControl(data[0].harian, Validators.required),
        tengah: new FormControl(data[0].semester_tengah, Validators.required),
        akhir: new FormControl(data[0].semester_akhir, Validators.required),
      });

    }, error => {
      console.error(error);
    });

    this.btn_submit_aktif = false;
  }

  ubah() {
    this.btn_submit_aktif = true;
  }

  simpan() {
    this.isSubmitted = true;

    if (this.myForm.valid) {
      let url = environment.server + 'setting_nas_update.php';
      let body = new FormData();
      body.append('harian', this.myForm.get('harian').value);
      body.append('tengah', this.myForm.get('tengah').value);
      body.append('akhir', this.myForm.get('akhir').value);

      this.http.post(url, body).subscribe((res: any) => {
        if (res.status) {
          this.global.notif_sukses("Berhasil mengupdate data.");
        } else {
          this.global.notif_error("Gagal mengupdate data.");
        }
      }, error => {
        console.error(error);
      });

    }
  }

}
