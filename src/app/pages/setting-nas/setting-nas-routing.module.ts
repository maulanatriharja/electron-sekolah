import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingNasPage } from './setting-nas.page';

const routes: Routes = [
  {
    path: '',
    component: SettingNasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingNasPageRoutingModule {}
