import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingNasPageRoutingModule } from './setting-nas-routing.module';

import { SettingNasPage } from './setting-nas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingNasPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [SettingNasPage]
})
export class SettingNasPageModule { }
