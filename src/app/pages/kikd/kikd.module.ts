import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KikdPageRoutingModule } from './kikd-routing.module';

import { KikdPage } from './kikd.page'; 

import { ComponentsModule } from '../../components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KikdPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
  ],
  declarations: [
    KikdPage
  ],
  entryComponents: [
  ],
  exports: []
})
export class KikdPageModule { }
