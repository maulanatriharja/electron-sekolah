import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController, ModalController } from '@ionic/angular';
import { FormControl, FormGroup, FormBuilder, Validators } from "@angular/forms";

import { environment } from '../../../../environments/environment';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-ki3',
  templateUrl: './ki3.component.html',
  styleUrls: ['./ki3.component.scss'],
})
export class Ki3Component implements OnInit {

  @Input() kelas: any;

  data: any = [];

  //modal
  openModal: boolean;
  id: string;

  isNewData: boolean;
  myForm: FormGroup;
  judul: string;
  data_mapel: any = [];
  isSubmitted = false;

  constructor(
    public alertController: AlertController,
    public formBuilder: FormBuilder,
    public global: GlobalService,
    public http: HttpClient,
    public modalController: ModalController,
  ) { }

  ngOnInit() { }

  ngOnChanges() {
    this.load_data();
    this.load_mapel();
  }

  load_data() {
    let url = environment.server + 'param_ki3_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data = data;
      } else {
        this.data = [];
      }
    }, error => {
      console.error(error);
    });
  }

  load_mapel() {
    let url = environment.server + 'mapel_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_mapel = data;

        this.load_data();
      }
    }, error => {
      console.error(error);
    });
  }

  async tambah() {
    this.isNewData = true;
    this.judul = "Tambah Parameter KI 3 - Kelas " + this.kelas

    this.myForm = new FormGroup({
      id_mapel: new FormControl('', Validators.required),
      kode: new FormControl('', Validators.required),
      aspek: new FormControl('', Validators.required),
    });
  }

  edit(val_id, val_id_mapel, val_kode, val_aspek) {
    if (val_id !== null) {
      this.openModal = true;

      this.isNewData = false;
      this.judul = "Edit Parameter KI 3 - Kelas " + this.kelas

      this.id = val_id;

      this.myForm = new FormGroup({
        id_mapel: new FormControl(val_id_mapel, Validators.required),
        kode: new FormControl(val_kode, Validators.required),
        aspek: new FormControl(val_aspek, Validators.required),
      });
    }
  }

  simpan() {
    this.isSubmitted = true;

    if (this.myForm.valid) {

      if (this.isNewData) { //tambah data
        let url = environment.server + 'param_ki3_insert.php';
        let body = new FormData();
        body.append('kelas', this.kelas);
        body.append('id_mapel', this.myForm.get('id_mapel').value);
        body.append('kode', this.myForm.get('kode').value);
        body.append('aspek', this.myForm.get('aspek').value);

        this.http.post(url, body).subscribe((res: any) => {
          if (res.status) {
            this.global.notif_sukses("Berhasil menambah data.").then(() => {
              this.modalController.dismiss();
              this.load_data();
              this.openModal = false;
            });
          } else {
            this.global.notif_error("Gagal menambah data.");
          }
        }, error => {
          console.error(error);
        });
      } else { //update data         
        let url = environment.server + 'param_ki3_update.php';
        let body = new FormData();
        body.append('id', this.id);
        body.append('kelas', this.kelas);
        body.append('id_mapel', this.myForm.get('id_mapel').value);
        body.append('kode', this.myForm.get('kode').value);
        body.append('aspek', this.myForm.get('aspek').value);

        this.http.post(url, body).subscribe((res: any) => {
          if (res.status) {
            this.global.notif_sukses("Berhasil mengupdate data.").then(() => {
              this.modalController.dismiss();
              this.load_data();
              this.openModal = false;
            });
          } else {
            this.global.notif_error("Gagal mengupdate data.");
          }
        }, error => {
          console.error(error);
        });

      }
    }
  }

  async hapus() {
    const alert = await this.alertController.create({
      message: 'Yakin untuk menghapus data ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'custom-alert-cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Hapus',
          cssClass: 'custom-alert-danger',
          handler: () => {
            let url = environment.server + 'param_ki3_delete.php';
            let body = new FormData();
            body.append('id', this.id);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.global.notif_hapus("Berhasil menghapus data.").then(() => {
                  this.modalController.dismiss();
                  this.load_data();
                  this.openModal = false;
                });
              } else {
                this.global.notif_error("Gagal menambah data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present();
  }
}
