import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

import { environment } from '../../../../environments/environment';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-ki2',
  templateUrl: './ki2.component.html',
  styleUrls: ['./ki2.component.scss'],
})
export class Ki2Component implements OnInit {

  @Input() kelas: any;

  param_ki_2: any = [];

  constructor(
    public alertController: AlertController,
    public global: GlobalService,
    public http: HttpClient,
  ) { }

  ngOnInit() { }

  ngOnChanges() {
    this.load_ki_2();
  }

  load_ki_2() {
    let url = environment.server + 'param_ki2_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.param_ki_2 = data;
      } else {
        this.param_ki_2 = [];
      }
    }, error => {
      console.error(error);
    });
  }

  async tambah_ki_2() {
    const alert = await this.alertController.create({
      header: 'Tambah Aspek',
      mode: 'ios',
      inputs: [
        {
          name: 'aspek',
          type: 'text',
          placeholder: 'Aspek'
        }
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'param_ki2_insert.php';
            let body = new FormData();
            body.append('kelas', this.kelas);
            body.append('aspek', val.aspek);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_ki_2();
                this.global.notif_sukses("Berhasil menambah data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }

  async edit_ki_2(val_id, val_aspek) {
    const alert = await this.alertController.create({
      header: 'Edit Aspek',
      mode: 'ios',
      inputs: [
        {
          name: 'aspek',
          type: 'text',
          value: val_aspek,
          placeholder: 'Aspek'
        }
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'custom-alert-cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'Hapus',
          cssClass: 'custom-alert-danger',
          handler: async () => {
            const alert = await this.alertController.create({
              message: 'Yakin untuk menghapus data ?',
              buttons: [
                {
                  text: 'Batal',
                  role: 'cancel',
                  cssClass: 'custom-alert-cancel',
                  handler: () => {
                    console.log('Confirm Cancel');
                  }
                }, {
                  text: 'Hapus',
                  cssClass: 'custom-alert-danger',
                  handler: () => {
                    console.log('Confirm Okay');

                    let url = environment.server + 'param_ki2_delete.php';
                    let body = new FormData();
                    body.append('id', val_id);

                    this.http.post(url, body).subscribe((res: any) => {
                      if (res.status) {
                        this.load_ki_2();
                        this.global.notif_hapus("Berhasil menghapus data.");
                      }
                    }, error => {
                      console.error(error);
                    });
                  }
                }
              ]
            });

            await alert.present();
          }
        },
        {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'param_ki2_update.php';
            let body = new FormData();
            body.append('id', val_id);
            body.append('aspek', val.aspek);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_ki_2();
                this.global.notif_sukses("Berhasil mengubah data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }
}
