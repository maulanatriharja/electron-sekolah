import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

import { environment } from '../../../../environments/environment';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-ki5',
  templateUrl: './ki5.component.html',
  styleUrls: ['./ki5.component.scss'],
})
export class Ki5Component implements OnInit {

  @Input() kelas: any;

  data_ekstra: any = [];
  data_aspek: any = [];

  constructor(
    public alertController: AlertController,
    public global: GlobalService,
    public http: HttpClient,
  ) { }

  ngOnInit() { }

  ngOnChanges() {
    this.load_ekstra();
  }

  load_aspek() {
    let url = environment.server + 'param_ki5_select.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_aspek = data;
      } else {
        this.data_aspek = [];
      }
    }, error => {
      console.error(error);
    });
  }

  load_ekstra() {
    let url = environment.server + 'param_ki5_select_ekstra.php?kelas=' + this.kelas;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data_ekstra = data;
        this.load_aspek();
      } else {
        this.data_ekstra = [];
      }
    }, error => {
      console.error(error);
    });
  }

  async tambah_ki_5_aspek(val_id_ekstra) {
    const alert = await this.alertController.create({
      header: 'Tambah Aspek',
      mode: 'ios',
      inputs: [
        {
          name: 'aspek',
          type: 'text',
          placeholder: 'Aspek'
        }
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'param_ki5_insert.php';
            let body = new FormData();
            body.append('id_ekstra', val_id_ekstra);
            body.append('kelas', this.kelas);
            body.append('aspek', val.aspek);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_ekstra();
                this.global.notif_sukses("Berhasil menambah data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }

  async edit_ki_5_aspek(val_id, val_aspek) {
    const alert = await this.alertController.create({
      header: 'Edit Aspek',
      mode: 'ios',
      inputs: [
        {
          name: 'aspek',
          type: 'text',
          value: val_aspek,
          placeholder: 'Aspek'
        }
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'Hapus',
          cssClass: 'custom-alert-danger',
          handler: async () => {
            const alert = await this.alertController.create({
              message: 'Yakin untuk menghapus data ?',
              buttons: [
                {
                  text: 'Batal',
                  role: 'cancel',
                  cssClass: 'custom-alert-cancel',
                  handler: () => {
                    console.log('Confirm Cancel');
                  }
                }, {
                  text: 'Hapus',
                  cssClass: 'custom-alert-danger',
                  handler: () => {
                    console.log('Confirm Okay');

                    let url = environment.server + 'param_ki5_delete.php';
                    let body = new FormData();
                    body.append('id', val_id);

                    this.http.post(url, body).subscribe((res: any) => {
                      if (res.status) {
                        this.load_ekstra();
                        this.global.notif_hapus("Berhasil menghapus data.");
                      }
                    }, error => {
                      console.error(error);
                    });
                  }
                }
              ]
            });

            await alert.present();
          }
        },
        {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'param_ki5_update.php';
            let body = new FormData();
            body.append('id', val_id);
            body.append('aspek', val.aspek);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_ekstra();
                this.global.notif_sukses("Berhasil mengupdate data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }

  async tambah_ki_5_ekstra() {
    const alert = await this.alertController.create({
      header: 'Tambah Ekstrakurikuler',
      mode: 'ios',
      inputs: [
        {
          name: 'ekstrakurikuler',
          type: 'text',
          placeholder: 'Ekstrakurikuler'
        }
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'param_ki5_insert_ekstra.php';
            let body = new FormData();
            body.append('kelas', this.kelas);
            body.append('ekstra_nama', val.ekstrakurikuler);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_ekstra();
                this.global.notif_sukses("Berhasil menambah data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }

  async edit_ki_5_ekstra(val_id, val_ekstra_nama) {
    const alert = await this.alertController.create({
      header: 'Edit Ekstrakurikuler',
      mode: 'ios',
      inputs: [
        {
          name: 'ekstrakurikuler',
          type: 'text',
          value: val_ekstra_nama,
          placeholder: 'Ekstrakurikuler'
        }
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'Hapus',
          cssClass: 'custom-alert-danger',
          handler: async () => {
            const alert = await this.alertController.create({
              message: 'Yakin untuk menghapus data ?',
              buttons: [
                {
                  text: 'Batal',
                  role: 'cancel',
                  cssClass: 'custom-alert-cancel',
                  handler: () => {
                    console.log('Confirm Cancel');
                  }
                }, {
                  text: 'Hapus',
                  cssClass: 'custom-alert-danger',
                  handler: () => {
                    console.log('Confirm Okay');

                    let url = environment.server + 'param_ki5_delete_ekstra.php';
                    let body = new FormData();
                    body.append('id', val_id);

                    this.http.post(url, body).subscribe((res: any) => {
                      if (res.status) {
                        this.load_ekstra();
                        this.global.notif_hapus("Berhasil menghapus data.");
                      }
                    }, error => {
                      console.error(error);
                    });
                  }
                }
              ]
            });

            await alert.present();
          }
        },
        {
          text: 'Simpan',
          handler: (val) => {
            let url = environment.server + 'param_ki5_update_ekstra.php';
            let body = new FormData();
            body.append('id', val_id);
            body.append('ekstra_nama', val.ekstrakurikuler);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.load_ekstra();
                this.global.notif_sukses("Berhasil mengupdate data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present().then(() => {
      const firstInput: any = document.querySelector('ion-alert input');
      firstInput.focus();
      return;
    });
  }

}
