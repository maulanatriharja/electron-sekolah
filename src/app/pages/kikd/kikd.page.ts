import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-kikd',
  templateUrl: './kikd.page.html',
  styleUrls: ['./kikd.page.scss'],
})
export class KikdPage implements OnInit {
  @ViewChild('slides', { static: true }) slider: IonSlides;

  kelas: any = '1';
  segment: any = 0; 

  constructor(
  ) { }

  async segmentChanged(ev: any) {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  ngOnInit() {
  }

















}
