import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

import { File } from '@ionic-native/file/ngx';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-siswa',
  templateUrl: './siswa.page.html',
  styleUrls: ['./siswa.page.scss'],
})
export class SiswaPage implements OnInit {

  kelas: string = '';
  paralel: string = '';
  tahun: string = '';
  tahun_depan: number;

  filter: string = '';

  data: any = [];

  loading = true;

  lebar_col1: any = 0;
  tinggi_col1: any = 0;

  constructor(
    public file: File,
    public http: HttpClient,
    public platform: Platform,
    public router: Router,
  ) { }

  async ngOnInit() {
    this.kelas = '1';
    this.tahun = new Date().getFullYear().toString();
    this.tahun_depan = parseInt(this.tahun) + 1;
  }

  ionViewDidEnter() {
    this.load_data();
  }

  load_data() {
    let url = environment.server + 'siswa_select.php?filter=' + this.filter + '&kelas=' + this.kelas + '&paralel=' + this.paralel + '&tahun=' + this.tahun;

    this.http.get(url).subscribe((data: any) => {
      if (data) {
        this.data = data;
        this.loading = false;
      } else {
        this.data = [];
        this.loading = false;
      }
      // console.info(JSON.stringify(data));  
    }, error => {
      console.error(error);
    });

    var intv_x: number = 0;
    var intv_lebar = setInterval(() => {
      if (document.getElementById("lebar_col1")) {
        this.lebar_col1 = document.getElementById("lebar_col1").clientWidth;

        console.info('interpal');

        intv_x += 1;

        if (intv_x > 3) {
          clearInterval(intv_lebar)
        }
      }
    }, 100);

    var intv_y: number = 0;
    var intv_tinggi = setInterval(() => {
      if (document.getElementById("tinggi_col1")) {
        this.tinggi_col1 = document.getElementById("tinggi_col1").clientHeight;

        console.info('interpal');

        intv_y += 1;

        if (intv_y > 3) {
          clearInterval(intv_tinggi)
        }
      }
    }, 100);
  }

  setTahun() {
    this.tahun_depan = parseInt(this.tahun) + 1;
  }

  async detail(val) {
    let params = {
      queryParams: {
        kelas: this.kelas,
        paralel: this.paralel,
        tahun: this.tahun,
        data: JSON.stringify(val),
      }
    };

    this.router.navigate(['/siswa/siswa-detail'], params);
  }

  pilih(val) {
    // console.info(val);
  }

}
