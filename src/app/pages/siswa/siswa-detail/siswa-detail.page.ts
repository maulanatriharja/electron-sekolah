import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';

import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';


import { environment } from '../../../../environments/environment';
import { GlobalService } from './../../../services/global.service';


import * as moment from 'moment';

@Component({
  selector: 'app-siswa-detail',
  templateUrl: './siswa-detail.page.html',
  styleUrls: ['./siswa-detail.page.scss'],
})
export class SiswaDetailPage implements OnInit {
  @ViewChild('input1') input1;


  judul: string;

  id: string;
  kelas: string;
  paralel: string;
  tahun: number;

  nis: number;
  agama_nama: string = '';

  myForm: FormGroup;
  isSubmitted = false;
  isNewData: boolean;

  constructor(
    public alertController: AlertController,
    public formBuilder: FormBuilder,
    public global: GlobalService,
    public http: HttpClient,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public storage: Storage,
  ) {
    this.route.queryParams.subscribe(params => {

      this.kelas = params.kelas;
      this.paralel = params.paralel;
      this.tahun = parseInt(params.tahun);

      let data: any = JSON.parse(params.data);

      if (data.id) {
        this.judul = "Edit Siswa";
        this.id = data.id;
        this.isNewData = false;

        this.nis = data.nis;

        this.setAgama(data.agama_kode);

        this.myForm = this.formBuilder.group({
          // nis: [data.nis, Validators.required],
          nama: [data.nama, Validators.required],
          smt1_tinggi: [data.smt1_tinggi],
          smt1_berat: [data.smt1_berat],
          smt2_tinggi: [data.smt2_tinggi],
          smt2_berat: [data.smt2_berat],
          kesehatan_telinga: [data.kesehatan_telinga],
          kesehatan_mata: [data.kesehatan_mata],
          kesehatan_gigi: [data.kesehatan_gigi],
          kesehatan_lain: [data.kesehatan_lain],
          lahir_tempat: [data.lahir_tempat],
          lahir_tanggal: [data.lahir_tanggal],
          gender: [data.gender],
          agama_kode: [data.agama_kode],
          // agama_nama: [data.agama_nama],
          pendidikan_sebelum: [data.pendidikan_sebelum],
          alamat: [data.alamat],
          ayah_nama: [data.ayah_nama],
          ibu_nama: [data.ibu_nama],
          ayah_pekerjaan: [data.ayah_pekerjaan],
          ibu_pekerjaan: [data.ibu_pekerjaan],
          orangtua_alamat: [data.orangtua_alamat],
          wali_nama: [data.wali_nama],
          wali_pekerjaan: [data.wali_pekerjaan],
          wali_alamat: [data.wali_alamat],
        });

        console.info("update data");
      } else {
        this.judul = "Tambah Siswa";
        this.id = moment().format('YYMMDDHHmmss');
        this.isNewData = true;

        this.myForm = this.formBuilder.group({
          nis: ['', Validators.required],
          nama: ['', Validators.required],
          smt1_tinggi: [''],
          smt1_berat: [''],
          smt2_tinggi: [''],
          smt2_berat: [''],
          kesehatan_telinga: [''],
          kesehatan_mata: [''],
          kesehatan_gigi: [''],
          kesehatan_lain: [''],
          lahir_tempat: [''],
          lahir_tanggal: ['0000-00-00'],
          gender: [''],
          agama_kode: [''],
          // agama_nama: [''],
          pendidikan_sebelum: [''],
          alamat: [''],
          ayah_nama: [''],
          ibu_nama: [''],
          ayah_pekerjaan: [''],
          ibu_pekerjaan: [''],
          orangtua_alamat: [''],
          wali_nama: [''],
          wali_pekerjaan: [''],
          wali_alamat: [''],
        });

        console.info("new data");
      }

    });
  }

  async ngOnInit() {
    setTimeout(() => { this.input1.setFocus(); }, 500);
  }

  autofill(val) {
    this.storage.get('siswa').then((res) => {
      let cek_siswa = res.filter(f => f.nis == val.value);

      if (cek_siswa.length > 0) {
        this.myForm = this.formBuilder.group({
          nama: [cek_siswa[0].nama, Validators.required],
          nis: [cek_siswa[0].nis, Validators.required],
          smt1_tinggi: [cek_siswa[0].smt1_tinggi],
          smt1_berat: [cek_siswa[0].smt1_berat],
          smt2_tinggi: [cek_siswa[0].smt2_tinggi],
          smt2_berat: [cek_siswa[0].smt2_berat],
          kesehatan_telinga: [cek_siswa[0].kesehatan_telinga],
          kesehatan_mata: [cek_siswa[0].kesehatan_mata],
          kesehatan_gigi: [cek_siswa[0].kesehatan_gigi],
          kesehatan_lain: [cek_siswa[0].kesehatan_lain],
          lahir_tempat: [cek_siswa[0].lahir_tempat],
          lahir_tanggal: [cek_siswa[0].lahir_tanggal],
          gender: [cek_siswa[0].gender],
          agama_kode: [cek_siswa[0].agama_kode],
          // agama_nama: [cek_siswa[0].agama_nama],
          pendidikan_sebelum: [cek_siswa[0].pendidikan_sebelum],
          alamat: [cek_siswa[0].alamat],
          ayah_nama: [cek_siswa[0].ayah_nama],
          ibu_nama: [cek_siswa[0].ibu_nama],
          ayah_pekerjaan: [cek_siswa[0].ayah_pekerjaan],
          ibu_pekerjaan: [cek_siswa[0].ibu_pekerjaan],
          orangtua_alamat: [cek_siswa[0].orangtua_alamat],
          wali_nama: [cek_siswa[0].wali_nama],
          wali_pekerjaan: [cek_siswa[0].wali_pekerjaan],
          wali_alamat: [cek_siswa[0].wali_alamat],
        });
      }
    })
  }

  setAgama(val) {
    if (val == 1) { this.agama_nama = 'Islam' }
    if (val == 2) { this.agama_nama = 'Kristen' }
    if (val == 3) { this.agama_nama = 'Katolik' }
    if (val == 4) { this.agama_nama = 'Hindu' }
    if (val == 5) { this.agama_nama = 'Budha' }
    if (val == 6) { this.agama_nama = 'Khonghucu' }
  }

  simpan() {
    this.isSubmitted = true;

    if (this.myForm.valid) {

      if (this.isNewData) { //tambah data
        let url = environment.server + 'siswa_insert.php';
        let body = new FormData();
        body.append('nis', this.myForm.get('nis').value);
        body.append('nama', this.myForm.get('nama').value);
        body.append('smt1_tinggi', this.myForm.get('smt1_tinggi').value);
        body.append('smt1_berat', this.myForm.get('smt1_berat').value);
        body.append('smt2_tinggi', this.myForm.get('smt2_tinggi').value);
        body.append('smt2_berat', this.myForm.get('smt2_berat').value);
        body.append('kesehatan_telinga', this.myForm.get('kesehatan_telinga').value);
        body.append('kesehatan_mata', this.myForm.get('kesehatan_mata').value);
        body.append('kesehatan_gigi', this.myForm.get('kesehatan_gigi').value);
        body.append('kesehatan_lain', this.myForm.get('kesehatan_lain').value);
        body.append('lahir_tempat', this.myForm.get('lahir_tempat').value);
        body.append('lahir_tanggal', this.myForm.get('lahir_tanggal').value);
        body.append('gender', this.myForm.get('gender').value);
        body.append('agama_kode', this.myForm.get('agama_kode').value);
        body.append('agama_nama', this.agama_nama);
        body.append('pendidikan_sebelum', this.myForm.get('pendidikan_sebelum').value);
        body.append('alamat', this.myForm.get('alamat').value);
        body.append('ayah_nama', this.myForm.get('ayah_nama').value);
        body.append('ibu_nama', this.myForm.get('ibu_nama').value);
        body.append('ayah_pekerjaan', this.myForm.get('ayah_pekerjaan').value);
        body.append('ibu_pekerjaan', this.myForm.get('ibu_pekerjaan').value);
        body.append('orangtua_alamat', this.myForm.get('orangtua_alamat').value);
        body.append('wali_nama', this.myForm.get('wali_nama').value);
        body.append('wali_pekerjaan', this.myForm.get('wali_pekerjaan').value);
        body.append('wali_alamat', this.myForm.get('wali_alamat').value);

        this.http.post(url, body).subscribe((res: any) => {
          if (res.status) {

            let url = environment.server + 'grup_insert.php';
            let body = new FormData();
            body.append('kelas', this.kelas);
            body.append('paralel', this.paralel);
            body.append('tahun', this.tahun.toString());
            body.append('nis', this.myForm.get('nis').value);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.global.notif_sukses("Berhasil menambah data.").then(() => this.navCtrl.back());
              } else {
                this.global.notif_error("Gagal menambah data.");
              }
            });

          } else {
            this.global.notif_error("Gagal menambah data.");
          }
        }, error => {
          console.error(error);
        });
      } else { //update data         
        let url = environment.server + 'siswa_update.php';
        let body = new FormData();
        body.append('id', this.id);
        body.append('nis', this.nis.toString());
        body.append('nama', this.myForm.get('nama').value);
        body.append('smt1_tinggi', this.myForm.get('smt1_tinggi').value);
        body.append('smt1_berat', this.myForm.get('smt1_berat').value);
        body.append('smt2_tinggi', this.myForm.get('smt2_tinggi').value);
        body.append('smt2_berat', this.myForm.get('smt2_berat').value);
        body.append('kesehatan_telinga', this.myForm.get('kesehatan_telinga').value);
        body.append('kesehatan_mata', this.myForm.get('kesehatan_mata').value);
        body.append('kesehatan_gigi', this.myForm.get('kesehatan_gigi').value);
        body.append('kesehatan_lain', this.myForm.get('kesehatan_lain').value);
        body.append('lahir_tempat', this.myForm.get('lahir_tempat').value);
        body.append('lahir_tanggal', this.myForm.get('lahir_tanggal').value);
        body.append('gender', this.myForm.get('gender').value);
        body.append('agama_kode', this.myForm.get('agama_kode').value);
        body.append('agama_nama', this.agama_nama);
        body.append('pendidikan_sebelum', this.myForm.get('pendidikan_sebelum').value);
        body.append('alamat', this.myForm.get('alamat').value);
        body.append('ayah_nama', this.myForm.get('ayah_nama').value);
        body.append('ibu_nama', this.myForm.get('ibu_nama').value);
        body.append('ayah_pekerjaan', this.myForm.get('ayah_pekerjaan').value);
        body.append('ibu_pekerjaan', this.myForm.get('ibu_pekerjaan').value);
        body.append('orangtua_alamat', this.myForm.get('orangtua_alamat').value);
        body.append('wali_nama', this.myForm.get('wali_nama').value);
        body.append('wali_pekerjaan', this.myForm.get('wali_pekerjaan').value);
        body.append('wali_alamat', this.myForm.get('wali_alamat').value);

        this.http.post(url, body).subscribe((res: any) => {
          if (res.status) {
            this.global.notif_sukses("Berhasil mengupdate data.").then(() => this.navCtrl.back());
          } else {
            this.global.notif_error("Gagal mengupdate data.");
          }
        }, error => {
          console.error(error);
        });

      }
    }
  }

  async hapus() {
    const alert = await this.alertController.create({
      header: 'Konfirmasi',
      message: 'Yakin untuk menghapus data siswa <strong>' + this.myForm.get('nama').value + '</strong> ?',
      mode: 'ios',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: (val) => {
          }
        }, {
          text: 'Hapus',
          handler: () => {
            let url = environment.server + 'siswa_delete.php';
            let body = new FormData();
            body.append('id', this.id);

            this.http.post(url, body).subscribe((res: any) => {
              if (res.status) {
                this.global.notif_hapus("Berhasil menghapus data.").then(() => this.navCtrl.back());
              } else {
                this.global.notif_error("Gagal menghapus data.");
              }
            }, error => {
              console.error(error);
            });
          }
        }
      ]
    });

    await alert.present();


  }



}
