import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SiswaDetailPageRoutingModule } from './siswa-detail-routing.module';

import { SiswaDetailPage } from './siswa-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SiswaDetailPageRoutingModule
  ],
  declarations: [SiswaDetailPage]
})
export class SiswaDetailPageModule { }
