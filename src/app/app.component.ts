import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public menu_utama = [
    { title: 'Data Siswa', url: '/siswa', icon: 'people' },
    // { title: 'Data Kelas', url: '/kelas', icon: 'easel' },
    { title: 'Mata Pelajaran & KKM', url: '/mapel', icon: 'library' },
    { title: 'KI KD', url: '/kikd', icon: 'file-tray-full' },
    { title: 'Kriteria Penilaian', url: '/kriteria-nilai', icon: 'layers' },
    // { title: 'KKM', url: '/kkm', icon: 'file-tray' },
  ];

  public menu_sekunder = [
    { title: 'Penilaian', url: '/penilaian', icon: 'book' },
    // { title: 'Penilaian K1', url: '/penilaian-k1', icon: 'book' },
    // { title: 'Penilaian K2', url: '/penilaian-k2', icon: 'book' },
    // { title: 'Penilaian k3', url: '/penilaian-k3', icon: 'book' },
    // { title: 'Penilaian k4', url: '/penilaian-k4', icon: 'book' },
    // { title: 'Penilaian k5', url: '/penilaian-k5', icon: 'book' },
  ];

  public menu_rapor = [
    { title: 'Rapor', url: '/rapor', icon: 'document-text' },
  ];

  public menu_pengaturan = [
    { title: 'Setting Penilaian', url: '/penilaian-setting', icon: 'checkbox-outline' },
    { title: 'Setting Rumus Nilai', url: '/setting-nas', icon: 'prism' },
    { title: 'Sekolah', url: '/sekolah', icon: 'business' },
    { title: 'Guru', url: '/guru', icon: 'school' },
    // { title: 'Kelas', url: '/kelas', icon: 'grid' },
  ];

  constructor() { }
}
