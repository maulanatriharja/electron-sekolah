import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'siswa',
    pathMatch: 'full'
  },
  {
    path: 'siswa',
    loadChildren: () => import('./pages/siswa/siswa.module').then(m => m.SiswaPageModule)
  },
  {
    path: 'kelas',
    loadChildren: () => import('./pages/kelas/kelas.module').then(m => m.KelasPageModule)
  },
  {
    path: 'sekolah',
    loadChildren: () => import('./pages/sekolah/sekolah.module').then(m => m.SekolahPageModule)
  },
  {
    path: 'guru',
    loadChildren: () => import('./pages/guru/guru.module').then(m => m.GuruPageModule)
  },
  {
    path: 'kkm',
    loadChildren: () => import('./pages/kkm/kkm.module').then(m => m.KkmPageModule)
  }, {
    path: 'kikd',
    loadChildren: () => import('./pages/kikd/kikd.module').then(m => m.KikdPageModule)
  },
  {
    path: 'penilaian',
    loadChildren: () => import('./pages/penilaian/penilaian.module').then(m => m.PenilaianPageModule)
  },
  {
    path: 'rapor',
    loadChildren: () => import('./pages/rapor/rapor.module').then(m => m.RaporPageModule)
  },
  {
    path: 'mapel',
    loadChildren: () => import('./pages/mapel/mapel.module').then(m => m.MapelPageModule)
  },
  {
    path: 'kriteria-nilai',
    loadChildren: () => import('./pages/kriteria-nilai/kriteria-nilai.module').then(m => m.KriteriaNilaiPageModule)
  },  {
    path: 'penilaian-setting',
    loadChildren: () => import('./pages/penilaian-setting/penilaian-setting.module').then( m => m.PenilaianSettingPageModule)
  },
  {
    path: 'setting-nas',
    loadChildren: () => import('./pages/setting-nas/setting-nas.module').then( m => m.SettingNasPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
