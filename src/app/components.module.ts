import { Component, NgModule } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PdfViewerModule } from "ng2-pdf-viewer";
import { RouterModule } from '@angular/router';

import { Ki1Component } from './pages/kikd/ki1/ki1.component';
import { Ki2Component } from './pages/kikd/ki2/ki2.component';
import { Ki3Component } from './pages/kikd/ki3/ki3.component';
import { Ki4Component } from './pages/kikd/ki4/ki4.component';
import { Ki5Component } from './pages/kikd/ki5/ki5.component';

import { Ki1NilaiComponent } from './pages/penilaian/ki1-nilai/ki1-nilai.component';
import { Ki2NilaiComponent } from './pages/penilaian/ki2-nilai/ki2-nilai.component';
import { Ki3NilaiComponent } from './pages/penilaian/ki3-nilai/ki3-nilai.component';
import { Ki4NilaiComponent } from './pages/penilaian/ki4-nilai/ki4-nilai.component';
import { Ki5NilaiComponent } from './pages/penilaian/ki5-nilai/ki5-nilai.component';

import { RaporCoverComponent } from './pages/rapor/rapor-cover/rapor-cover.component';
import { RaporSemesterTengahComponent } from './pages/rapor/rapor-semester-tengah/rapor-semester-tengah.component';
import { RaporSemesterAkhirComponent } from './pages/rapor/rapor-semester-akhir/rapor-semester-akhir.component';

@NgModule({
    declarations: [
        Ki1Component,
        Ki2Component,
        Ki3Component,
        Ki4Component,
        Ki5Component,

        Ki1NilaiComponent,
        Ki2NilaiComponent,
        Ki3NilaiComponent,
        Ki4NilaiComponent,
        Ki5NilaiComponent,

        RaporCoverComponent,
        RaporSemesterTengahComponent,
        RaporSemesterAkhirComponent,
    ],
    exports: [
        Ki1Component,
        Ki2Component,
        Ki3Component,
        Ki4Component,
        Ki5Component,

        Ki1NilaiComponent,
        Ki2NilaiComponent,
        Ki3NilaiComponent,
        Ki4NilaiComponent,
        Ki5NilaiComponent,

        RaporCoverComponent,
        RaporSemesterTengahComponent,
        RaporSemesterAkhirComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        PdfViewerModule,
        ReactiveFormsModule,
        RouterModule,
    ]
})

export class ComponentsModule { }