import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  subject = new Subject<any>();

  constructor() { }

  // pubNilai(data: any) { this.subject.next(data); }
  // obvNilai(): Subject<any> { return this.subject; }
}
