import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(
    public toastController: ToastController,
  ) { }

  async notif_sukses(msg) {
    const toast = await this.toastController.create({
      message: msg, 
      position: 'top',
      duration: 1500,
      cssClass: 'notif_sukses'
    });
    toast.present();
  }

  async notif_hapus(msg) {
    const toast = await this.toastController.create({
      message: msg, 
      position: 'top',
      duration: 1500,
      cssClass: 'notif_hapus'
    });
    toast.present();
  }

  async notif_error(msg) {
    const toast = await this.toastController.create({
      message: msg, 
      position: 'top',
      duration: 1500,
      cssClass: 'notif_error'
    });
    toast.present();
  }
}
